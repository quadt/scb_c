
/// @description Movement

if (!isDied){
	//Keys Init
	key_R = keyboard_check(vk_right);
	key_L = keyboard_check(vk_left);
	key_U = keyboard_check(vk_up);
	key_D = keyboard_check(vk_down);

	key_U_r = keyboard_check_released(vk_up);

	key_Space = keyboard_check_pressed(vk_space);
		//Directions:Weapons
	keyW = keyboard_check(ord("W"));
	keyA = keyboard_check(ord("A"));
	keyS = keyboard_check(ord("S"));
	keyD = keyboard_check(ord("D"));


	noKeys = (!key_R && !key_L) || (key_R && key_L)

	//Movement Inits
	isGrounded = place_meeting(x, y+1, objBlock);
	isJumpBlocked = place_meeting(x,y,objJumpBlockingTrig);
	
	//X Movement
	if (noKeys){
		if (XMove > 0){
			XMove = max(XMove - (XSpd*(XAcc*0.01)), 0);
		} else {
			XMove = min(XMove + (XSpd*(XAcc*0.01)), 0);
		}
	}
	
	if (key_R){
		MDir  = 1;
		XMove = min(XMove + (XSpd*(XAcc*0.01)), XMaxSpd+XPush);
	} else if (key_L) {
		MDir  = -1;
		XMove = max(XMove - (XSpd*(XAcc*0.01)), -XMaxSpd+XPush);
	}
	//PlayerPush when Fire
	if (XPush < 0){
		XPush = min(XPush + (XSpd*(XAcc*0.01)), 0);
	} else {
		XPush = max(XPush - (XSpd*(XAcc*0.01)), 0);
	}
	
	//Y Movement
	YMove += YGrav;
	
	if(isGrounded){	
		if(key_U && !isJumpBlocked){
			var d = instance_create_layer(x, y, "Player", objPJDust);
			d.image_xscale *= Dir;
			audio_play_sound(sfx.jump, 1, false);
			YMove = YJump;
		}
	} else if (key_U_r && YMove < 0){
		YMove *= 0.6;
	}

	//X Collisions
	if (place_meeting(x+(XMove), y, objBlock)){
		while(!place_meeting(x+sign(XMove), y, objBlock)){
			x += sign(XMove);
		}
		XMove = 0;
	}
	x += XMove;
	
	//Y Collisions
	if (place_meeting(x, y+YMove, objBlock)){
		while(!place_meeting(x, y+sign(YMove), objBlock)){
			y += sign(YMove);
		}
		YMove = 0;
	}
	y += YMove;
	
} else if (!ded){
	audio_play_sound(sndPlayerDeath, 3, false);
	YMove = -random_range(1.5, 3);
	
	scrDropWep(WDX, WDY, XMove+(random_range(1, 2)*Dir), -random_range(1.5, 3), Dir, WeaponSelected);
	
	ded = true;
} else {
	GENERAL.playerIsDied = true;
	YMove += YGrav;
	y += YMove;
	x += XMove;
}

//Weapons

anyKey = keyD || keyA || keyW || keyS;

if (WeaponSelected != weapon.discGun && WeaponSelected != weapon.laserRifle){
	d0 = keyD && !(keyA || keyW || keyS);	// 0
	d1 = (keyD && keyW) && !(keyS || keyA); // 45
	d2 = keyW && !(keyA || keyS || keyD);	// 90
	d3 = (keyW && keyA) && !(keyS || keyD); // 135
	d4 = keyA && !(keyW || keyS || keyD);	// 180
	d5 = (keyA && keyS) && !(keyW || keyD); // 225
	d6 = keyS && !(keyA || keyW || keyD);	// 270
	d7 = (keyS && keyD) && !(keyW || keyA);	// 315
} else {
	d0 = keyD && !(keyA || keyW || keyS);	// 0
	d1 = false; // 45
	d2 = false;	// 90
	d3 = false; // 135
	d4 = keyA && !(keyW || keyS || keyD);	// 180
	d5 = false; // 225
	d6 = false;	// 270
	d7 = false;	// 315
}

if (anyKey){
	keyDelay = max(keyDelay - 0.25, 0);
} else {
	keyDelay = 1;
}
if (keyDelay > 0){
	if (d0){
		WTargetAngle = 0.001;
	} else if (d1){
		WTargetAngle = 45;
	} else if (d2){
		WTargetAngle = 90;
	} else if (d3){
		WTargetAngle = 135;
	} else if (d4){
		WTargetAngle = 179.999;
	} else if (d5){
		WTargetAngle = 225;
	} else if (d6){
		WTargetAngle = 270;
	} else if (d7){
		WTargetAngle = 315;
	}
}

	//Weapon Movement
WTargetXPos	= dcos(WTargetAngle)*WDistFromP;
WTargetYPos	= dsin(WTargetAngle)*WDistFromP/1.8;

WAngle      = WAngle - angle_difference(WAngle, WTargetAngle) * 0.2;
WXPos	    = WXPos + (WTargetXPos - WXPos) * 0.2;
WYPos	    = WYPos + (WTargetYPos - WYPos) * 0.2;

WDistFromP  = 6;

//Pistol YScale orientation
if (WTargetAngle >= 360){
	WTargetAngle -= 360;
} else if (WTargetAngle < 0){
	WTargetAngle += 360;
}

if ((x+WTargetXPos) >= x){
	Dir = 1;
} else if ((x+WTargetXPos) < x){
	Dir = -1;
}

if (place_meeting(x, y, objPlayerKillTrig) && !isDied){	
	isDied = true;
}































