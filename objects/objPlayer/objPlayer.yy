{
    "id": "32b47ebc-1e14-4a7f-b3dd-adf625cc5d64",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPlayer",
    "eventList": [
        {
            "id": "8fbfba49-fb93-4171-bf14-589e034447b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32b47ebc-1e14-4a7f-b3dd-adf625cc5d64"
        },
        {
            "id": "11a58778-8a62-4df5-9826-6ac8366279d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32b47ebc-1e14-4a7f-b3dd-adf625cc5d64"
        },
        {
            "id": "c9236fc4-eaad-4b83-b138-61a24f171202",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "32b47ebc-1e14-4a7f-b3dd-adf625cc5d64"
        },
        {
            "id": "c3e9dd58-c3ab-42ee-956d-f6df325112a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "32b47ebc-1e14-4a7f-b3dd-adf625cc5d64"
        },
        {
            "id": "3dcd0a9c-5d44-4448-b914-2f7c4373fea3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "eb9f9bea-5d5a-4141-a5a4-704839c0d5e9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "32b47ebc-1e14-4a7f-b3dd-adf625cc5d64"
        },
        {
            "id": "190fa39b-e498-4702-980c-9015aaf6630a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "60524cbd-e844-4cf9-a69a-238f57ca2853",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "32b47ebc-1e14-4a7f-b3dd-adf625cc5d64"
        },
        {
            "id": "de84375d-26a0-4d6a-9bb5-e5fd37d7a57e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "37c797ba-f40f-4550-ba41-948de6f3f786",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "32b47ebc-1e14-4a7f-b3dd-adf625cc5d64"
        }
    ],
    "maskSpriteId": "4ef313eb-c92d-4a60-9160-40654a710930",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4ef313eb-c92d-4a60-9160-40654a710930",
    "visible": true
}