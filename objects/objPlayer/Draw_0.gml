
/// @description Draw stuff
if(global.DEBUG){
	draw_self();
}

//Animations

if (!isDied){
	animSpd = (animCurSpd/10)*animRev;
	animRev = MDir / Dir;

	if(!isGrounded){
		animCurSpd = 0;
		rotDelay--;
		if (rotDelay <= 0){
			animAngle -= 90*Dir;
			rotDelay = rotDelayDef;
		}
		animFrame = 0;	
		curAnim = anim.jump;
		if (animAngle >= 360) {
			animAngle = 0
		}
	} else {
		animAngle = 0;
		if (abs(XMove-XPush) < 0.1) {
			animCurSpd = 1;
			curAnim = anim.idle;
		} else if (abs(XMove-XPush) >= 0.1){
			animCurSpd = 2;
			curAnim = anim.run;
		}	
	}

	animFrame += animSpd;
	if (animFrame >= sprite_get_number(curAnim)) {
		animFrame = 0;
	}
	if (animFrame < 0){
		animFrame = sprite_get_number(curAnim);
	}
} else {
	animFrame = 0;
	curAnim = anim.idle;
	animAngle += 5;
}

if(!global.DEBUG){
	draw_sprite_ext(curAnim, floor(animFrame), x, y, 1*Dir, 1, animAngle, c_white, 1);
}

//Weapons
if (!isDied){
	var sx, sy, bx, by;
	pushMult = 1;
	if (WeaponSelected == weapon.pistol){
		sx = x+WXPos;
		sy = y-WYPos;
		bx = x+WXPos*1.5;
		by = y-WYPos*1.5;
	} else if (WeaponSelected == weapon.shotgun){
		sx = x+WXPos*0.35;
		sy = (y+2)-WYPos;
		bx = x+WXPos*1.5;
		by = (y+2)-WYPos*3;
	} else if (WeaponSelected == weapon.machineGun){
		sx = x+WXPos*0.02;
		sy = (y+1)-WYPos*0.5;
		bx = x+WXPos*1.5;
		by = y-WYPos*3;
	} else if (WeaponSelected == weapon.revolver){
		sx = x+WXPos;
		sy = y-WYPos;
		bx = x+WXPos*1.5;
		by = (y-1)-WYPos*1.5;
	} else if (WeaponSelected == weapon.discGun){
		sx = x+WXPos*0.5;
		sy = y-WYPos*0.5;
		bx = x+WXPos*1.5;
		by = y-WYPos*1.5;
	} else if (WeaponSelected == weapon.laserRifle){
		sx = x+WXPos*0.35;
		sy = (y+1)-WYPos*0.35;
		bx = sx + lengthdir_x(8, WAngle);
		by = (sy-1) + lengthdir_y(4, WAngle);
	}
	WDX = sx;
	WDY = sy;
	BDX = bx;
	BDY = by;
		//Shoting
	if(isGrounded){
		pushMult = 1;
	} else {
		pushMult = 2;
	}
	
	//Weapon Shoting System
	isWepFastShoting = WeaponSelected == weapon.machineGun;
	if (isWepFastShoting){
		key_Space = keyboard_check(vk_space);
	}
	if (key_Space && reloaded){
		if (WeaponSelected == weapon.pistol){
			audio_play_sound(wepSFX.defShot, 2, false);
			scrShot(bx, by, 5, WAngle, "long", 1, 5, sprBullet);
			scrShellCreate(sx, sy,XMove+(random_range(1, 2)*-Dir) + (random_range(1, 2)*-Dir)*abs(dsin(WAngle)),YMove+random_range(-1.5, -1)*abs(dcos(WAngle)), 1);
			
		} else if (WeaponSelected == weapon.shotgun){
			audio_play_sound(wepSFX.shotgunShot, 2, false);
			for (i = 0; i < 5; i++){
				scrShot(bx, by, 7+(abs(XMove)*max(animRev,0)*abs(dcos(WAngle))), WAngle+(random_range(-10,20)*Dir), "short", 2, 5, sprBullet);
			}
			WAngPush += random(8); 		
			scrPush((random_range(1, 2)*-Dir*pushMult)*abs(dcos(WAngle)), (random_range(1, 2))*dsin(WAngle));
			
			reloaded    = false;
			reloadDelay = 30;
			
		} else if (WeaponSelected == weapon.machineGun){
			audio_play_sound(wepSFX.defShot, 2, false); 
			
			scrShot(bx, by, 5, WAngle+random_range(-4,4), "long", 1.5, 5, sprBullet);
			scrScreenShake(2.5, 2.5);
			WAngPush += random_range(-8,8); 
			scrPush((random_range(0, 0.8)*-Dir*pushMult)*abs(dcos(WAngle)), (random_range(0, 0.8))*dsin(WAngle));
			reloaded    = false;
			reloadDelay = 5;
			
		} else if (WeaponSelected == weapon.revolver){
			audio_play_sound(wepSFX.revShot, 2, false);
			scrShot(bx, by, 5, WAngle, "long", 2, 25, sprRevBullet);
			
		} else if (WeaponSelected == weapon.discGun){
			audio_play_sound(wepSFX.discGunShot, 2, false);
			scrShot(bx, by, 5, WTargetAngle, "disc", 2, 25, sprDisc);
			WPush = 2;
			scrPush((random_range(0.5, 1)*-Dir*pushMult)*abs(dcos(WAngle)), 0);
			
		} else if (WeaponSelected == weapon.laserRifle && !(instance_exists(objRlaser_Shot) || instance_exists(objRlaser_Shot))){
			audio_play_sound(wepSFX.LRifleCharge, 2, false);
			b = instance_create_layer(bx, by, "Bullets", objRlaser_Show);
			reloadDelay = 50;
			reloaded = false;
		}
	}
	
	WPush = clamp(WPush+(0-WPush)*0.25, 0, 2);
	WAngPush = WAngPush + angle_difference(0, WAngPush)*0.25
	
	//Weapon Reloading System
	reloadDelay = max(reloadDelay - 1, 0);
	if (reloadDelay == 0 && !reloaded){
		if (WeaponSelected == weapon.pistol){
			
		}else if (WeaponSelected == weapon.shotgun){
			audio_play_sound(wepSFX.shotgunReload, 13, false);	
			scrShellCreate(sx, sy,XMove+(random_range(1, 2)*-Dir) + (random_range(1, 2)*-Dir)*abs(dsin(WAngle)),YMove+random_range(-1.5, -1)*abs(dcos(WAngle)), 0);
		} else if (WeaponSelected == weapon.machineGun){
			scrShellCreate(sx, sy,XMove+(random_range(1, 2)*-Dir) + (random_range(1, 2)*-Dir)*abs(dsin(WAngle)),YMove+random_range(-1.5, -1)*abs(dcos(WAngle)), 1);
		} else if (WeaponSelected == weapon.laserRifle){
			audio_play_sound(wepSFX.LRifleShot, 2, false);
			with (objRlaser_Show){instance_destroy();}
			b = instance_create_layer(bx, by, "Bullets", objRlaser_Shot);
			b.Dir = Dir;
			b.image_angle = WTargetAngle;
			WPush = 2;
			scrScreenShake(3.5, 3.5);
			scrPush((random_range(1, 2)*-Dir*pushMult)*abs(dcos(WAngle)), 0);
		}
		reloaded = true;
	} else if (reloadDelay <> 0){
		if (WeaponSelected == weapon.laserRifle){
			draw_sprite_ext(sprLRLaser, 0, bx, by, 1, 1*Dir, WAngle, c_white, 1);
		}
	}
	
	draw_sprite_ext(WeaponSelected, 0, sx-(WPush*dcos(WAngle)), sy-(WPush*-dsin(WAngle)), 1, 1*Dir, WAngle+WAngPush, c_white, 1);
}


//draw_text(x+10, y-10, instance_number(objShell));
























