/// @description Init

scrSFXFogCreate(x, y);

//Movement
Dir      = 1;
MDir	 = 1;
XMove	 = 0;
YMove	 = 0;
XSpd	 = 2;
XMaxSpd  = XSpd;
XPush    = 0;
pushMult = 1;
XAcc	 = 10;
YJump    = -3.8;
YGrav    = 0.2;

isDied   = false;
ded = false;

//Weapons
WXPos	     = 3;
WYPos		 = -3;
WTargetXPos	 = 0;
WTargetYPos	 = 0;
WDir	     = Dir;
WAngle	     = 315;
WTargetAngle = 0;
WDistFromP   = 0;
WPush        = 0;
WAngPush	 = 0;
WPushDec     = 0.25;

reloadDelay = 0;
reloaded    = true;

WDX = 0;
WDY = 0;

//Controls
keyDelay = 1;

//Animation
animAngle   = 0;
animFrame   = 0;
animCurSpd  = 1;
animRev		= 1;
animSpd     = (animCurSpd/10)*animRev;
curAnim     = 0;
rotDelayDef = 5;
rotDelay    = rotDelayDef;

//Animation Consts
enum anim{
	idle = sprPlayer_Idle,
	run  = sprPlayer_Runing,
	jump = sprPlayer_Jumping
}

