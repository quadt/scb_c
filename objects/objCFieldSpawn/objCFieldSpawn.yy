{
    "id": "3738eed3-55ee-4d18-992e-7639defe5abd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCFieldSpawn",
    "eventList": [
        {
            "id": "b0382ba5-1313-415a-be41-4b6eab038807",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3738eed3-55ee-4d18-992e-7639defe5abd"
        },
        {
            "id": "3073172a-f1aa-4ecf-a990-754e8ba1a261",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3738eed3-55ee-4d18-992e-7639defe5abd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "f62c6120-b3e9-45b1-9f98-99144a11b470",
    "visible": false
}