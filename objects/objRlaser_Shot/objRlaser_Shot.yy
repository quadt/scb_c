{
    "id": "13d8ff82-dbe1-4a9e-bf37-df2f974e3336",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objRlaser_Shot",
    "eventList": [
        {
            "id": "85f6831f-a159-4bce-aa8f-a2453e7d935a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "13d8ff82-dbe1-4a9e-bf37-df2f974e3336"
        },
        {
            "id": "1870772a-7e9c-4b56-b626-cf1fddd90d64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13d8ff82-dbe1-4a9e-bf37-df2f974e3336"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "37c797ba-f40f-4550-ba41-948de6f3f786",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "ba631f13-6eec-4994-a121-02fbce599084",
    "visible": true
}