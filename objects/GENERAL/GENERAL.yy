{
    "id": "b9aec477-3d7d-4a02-83bc-17a76f05249a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GENERAL",
    "eventList": [
        {
            "id": "4c96e3e4-b94a-4801-933c-085357f1a1b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b9aec477-3d7d-4a02-83bc-17a76f05249a"
        },
        {
            "id": "d8992729-0b94-4b49-a065-3912c23dfbf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b9aec477-3d7d-4a02-83bc-17a76f05249a"
        },
        {
            "id": "72b5ad07-45c8-4033-97b4-009d28bb8846",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b9aec477-3d7d-4a02-83bc-17a76f05249a"
        },
        {
            "id": "1bc8a2e2-54e7-49e3-8b8f-bc48d17e999a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b9aec477-3d7d-4a02-83bc-17a76f05249a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}