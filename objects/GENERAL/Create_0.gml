/// @description Init Globals

randomize();
//DEBUG
global.DEBUG         = false;
global.disableEnemys = false;

playerIsDied = false;
enum weapon{
	pistol     = sprPistol,
	shotgun    = sprShotgun,
	machineGun = sprMachineGun,
	revolver   = sprRevolver,
	discGun    = sprDiscGun,
	laserRifle = sprLaserRifle
}
enum wepSFX{
	defShot       = sndPlayerShot,
	revShot       = sndRevolverShot,
	
	shotgunShot   = sndShotgunShot,
	shotgunReload = sndShotgunReload,
	discGunShot   = sndDiscGunShot,
	discGunBounce = sndDiscGunBounce,
	
	LRifleCharge  = sndLRifleCharging,
	LRifleShot    = sndLRifleShot
	
}
objPlayer.WeaponSelected = weapon.pistol;

//SFX
enum sfx{
	jump		  = sndPlayerJump,
	enHit		  = sndEnHit,
	enDeath		  = sndEnDeath,
	bigEnDeath    = sndBigEnDeath,
	enAnger		  = sndEnAngery,
	bigEnGrounded = sndBigEnGrounded
}

var f  = instance_find(objCFieldSpawn, irandom(instance_number(objCFieldSpawn) - 1));
var W  = f.sprite_width/2;
var H  = f.sprite_height/2;
var rX = random_range(f.x-W, f.x+W);
var rY = random_range(f.y-H, f.y+H);
instance_create_layer(rX, rY, "Player", objCrate);

enSpawnDelay = irandom_range(5, 60*3);

//Scores
Crates = 0;










