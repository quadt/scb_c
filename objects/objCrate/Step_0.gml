/// @description Movement

YMove += YGrav;

if(place_meeting(x, y+YMove, objBlock)){
	while(!place_meeting(x, y+sign(YMove), objBlock)){
		y += sign(YMove);
	}
	YMove = 0;
}
y += YMove;

//Pickuping Crate
if (place_meeting(x, y, objPlayer)){
	audio_play_sound(sndCratePick, 5, false);
	var p = objPlayer
	
	scrDropWep(p.WDX, p.WDY, p.XMove+(random_range(1, 2)*p.Dir), -random_range(1.5, 3), p.Dir, p.WeaponSelected);
	
	p.reloaded       = true;
	p.WXPos	         = 3;
	p.WYPos		     = -3;
	p.WAngle	     = 270 + 45 * p.Dir;
	
	scrWepRNGSelection();
	GENERAL.Crates++;
	instance_destroy();
}