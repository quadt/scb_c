{
    "id": "0c09479c-0cae-4c43-ad23-e368be603c5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCrate",
    "eventList": [
        {
            "id": "fea8eeeb-c213-42bc-ab16-1a1b947a339d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c09479c-0cae-4c43-ad23-e368be603c5f"
        },
        {
            "id": "cfc490a2-8ad4-4684-a4fa-9bdd2c918a73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0c09479c-0cae-4c43-ad23-e368be603c5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "71a9f925-d5f7-4788-b7f3-d70233cf6a14",
    "visible": true
}