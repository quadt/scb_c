/// @description Movement

xShake = max(xShake - shakeDec, 0);
yShake = max(yShake - shakeDec, 0);

view_xport[0] = 0 + random_range(xShake*-2, xShake*2);
view_yport[0] = 0 + random_range(yShake*-2, yShake*2);
