{
    "id": "6e01c186-f1b9-4170-a1b2-fd3fd9d1f4dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objRlaser_Show",
    "eventList": [
        {
            "id": "88686269-59af-44a6-bc9b-0c7c09755b13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6e01c186-f1b9-4170-a1b2-fd3fd9d1f4dc"
        },
        {
            "id": "f42d627d-a5af-4516-a28e-b2cb68abb137",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e01c186-f1b9-4170-a1b2-fd3fd9d1f4dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "37c797ba-f40f-4550-ba41-948de6f3f786",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "de6ca737-9d36-4189-af50-75df78761d13",
    "visible": false
}