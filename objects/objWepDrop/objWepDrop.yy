{
    "id": "e4280bb9-5efc-4968-9027-b05f3257bd92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objWepDrop",
    "eventList": [
        {
            "id": "14b4500a-f5a4-48b3-960a-fdb5d4b1dd94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e4280bb9-5efc-4968-9027-b05f3257bd92"
        },
        {
            "id": "ed6bdef1-d137-40c5-a566-f6068443c000",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e4280bb9-5efc-4968-9027-b05f3257bd92"
        },
        {
            "id": "6f1ef582-30a5-4d1c-a5fa-0caf54c0a488",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e4280bb9-5efc-4968-9027-b05f3257bd92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "86d4568d-7fc7-4760-a324-f090b5d0219a",
    "visible": true
}