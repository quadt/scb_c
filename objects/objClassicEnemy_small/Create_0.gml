/// @description Init

//HP
HP = 10;
Angery = false;

//Movement
isGrounded = false;

MDir = 0;
Dir = 1;
XSpd = 1.2;
XMove = 0;

YMove = 0;
YGrav = 0.2;

hitShowDelay = 0;
color        = c_white;

isDied   = false;
ded = false;

isDelayDamaged = false;

//Animation
animAngle   = 0;
animMAngle   = 0;
animFrame   = 0;
animCurSpd  = 1;
animSpd     = (animCurSpd/10);
curAnim     = 0;
rotDelayDef = 5;
rotDelay    = rotDelayDef;

//Animation Consts
enum EC_s_anim{
	run      = sprClassic_small_Run,
	angerRun = sprClassicAng_small_Run,
	fall     = sprClassic_small_Fall
}