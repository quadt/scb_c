/// @description Movement

hitShowDelay = max(hitShowDelay - 1, 0);

if (hitShowDelay > 0){
	color = c_red;
} else {
	color = c_white;
}

if (HP <= 0){
	isDied = true;
}

if (!isDied){
	isGrounded = place_meeting(x,y+1,objBlock);

	XMove = XSpd*Dir;
	YMove += YGrav;

	if (place_meeting(x+XMove, y, objBlock)){
		while(!place_meeting(x+sign(XMove), y, objBlock)){
			x += sign(XMove);
		}
		XMove = 0;
		Dir *= -1;
	}
	x += XMove;

	if (place_meeting(x, y+YMove, objBlock)){
		while(!place_meeting(x, y+sign(YMove), objBlock)){
			y += sign(YMove);
		}
		YMove = 0;
	}
	y += YMove;

} else if (!ded){
	audio_play_sound(sfx.enDeath, 11, false);
	animMAngle = random_range(-5, 5)
	YMove = -random_range(1.5, 3);
	
	ded = true;
} else {
	YMove += YGrav;
	y += YMove;
	x += XMove*MDir;
}

if (place_meeting(x, y, objPlayerKillTrig) && !isDied){
	if (!Angery){
		audio_play_sound(sfx.enAnger, 12, false);
		XSpd *=2;
	}
	Angery = true;
	y = -36;
}

var bp = objBullet;
if (instance_exists(bp)){
	if (bp.type == "disc" && !place_meeting(x, y, bp) && isDelayDamaged){
		isDelayDamaged = false;
	}
}



















