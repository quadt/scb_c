{
    "id": "eb9f9bea-5d5a-4141-a5a4-704839c0d5e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objClassicEnemy_small",
    "eventList": [
        {
            "id": "ba68308e-5ff6-4853-bf2f-a868180a150b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eb9f9bea-5d5a-4141-a5a4-704839c0d5e9"
        },
        {
            "id": "4de9d8e6-1812-4993-85ef-231545e2b483",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eb9f9bea-5d5a-4141-a5a4-704839c0d5e9"
        },
        {
            "id": "49d71f99-9014-4c50-9c25-bb16fe2c9675",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eb9f9bea-5d5a-4141-a5a4-704839c0d5e9"
        },
        {
            "id": "4d204b3e-1fd7-4d7a-921e-5d9efd2ab8bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "37c797ba-f40f-4550-ba41-948de6f3f786",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "eb9f9bea-5d5a-4141-a5a4-704839c0d5e9"
        },
        {
            "id": "cb9d077f-018a-4f21-baf0-3f9c9e77cb7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "eb9f9bea-5d5a-4141-a5a4-704839c0d5e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "89a3a88f-f5b2-4204-b38e-02b7499ab9c2",
    "visible": true
}