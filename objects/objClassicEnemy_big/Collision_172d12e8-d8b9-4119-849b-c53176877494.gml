if (!isDied){
	var MDDir = 0
	if (other.x < x){
		MDDir = 1;
	} else if (other.x >= x){
		MDDir = -1;
	}
	if (other.type == "long" || other.type == "short" ){
		MDir = Dir / MDDir;
		HP -= other.DMG;	
		hitShowDelay = 8;
		audio_play_sound(sfx.enHit, 10, false);
		instance_destroy(other);
		
	} else if (other.type == "RL" && other.isDMGle){
		MDir = Dir / MDDir;
		HP -= other.DMG;	
		hitShowDelay = 8;
		audio_play_sound(sfx.enHit, 10, false);

	} else if (other.type == "disc" && !isDelayDamaged){
		MDir = Dir / MDDir;
		HP -= other.DMG;	
		hitShowDelay = 8
		audio_play_sound(sfx.enHit, 10, false);
		
		isDelayDamaged = true;
	}
}