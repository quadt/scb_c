{
    "id": "60524cbd-e844-4cf9-a69a-238f57ca2853",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objClassicEnemy_big",
    "eventList": [
        {
            "id": "54dad2e1-8b8f-4679-b378-3aedd20bf9a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60524cbd-e844-4cf9-a69a-238f57ca2853"
        },
        {
            "id": "87495ab5-179a-496c-acfa-afa89833687b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "60524cbd-e844-4cf9-a69a-238f57ca2853"
        },
        {
            "id": "b4a77fe3-adc7-43ab-a7e1-129e390ac535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "60524cbd-e844-4cf9-a69a-238f57ca2853"
        },
        {
            "id": "172d12e8-d8b9-4119-849b-c53176877494",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "37c797ba-f40f-4550-ba41-948de6f3f786",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "60524cbd-e844-4cf9-a69a-238f57ca2853"
        },
        {
            "id": "e2446708-95e2-4d61-a9b2-c63ced420d8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "60524cbd-e844-4cf9-a69a-238f57ca2853"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "31b8757f-cc51-43ee-94e9-a4fb698f82be",
    "visible": true
}