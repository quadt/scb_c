/// @description Init

//HP
HP = 50;
Angery = false;

//Movement
gBegin	   = true;
isGrounded = false;
GSnd	   = false;

MDir = 0;
Dir = 1;
XSpd = 1.2;
XMove = 0;

YMove = 0;
YGrav = 0.4;

hitShowDelay = 0;
color        = c_white;

isDied   = false;
ded = false;

isDelayDamaged = false;
//Animation
animAngle   = 0;
animMAngle   = 0;
animFrame   = 0;
animCurSpd  = 1;
animSpd     = (animCurSpd/10);
curAnim     = 0;
rotDelayDef = 5;
rotDelay    = rotDelayDef;

//Animation Consts
enum EC_b_anim{
	run			= sprClassic_big_Run,
	angerRun	= sprClassicAng_big_Run,
	fall		= sprClassic_big_Fall,
	angerFall   = sprClassicAng_big_Fall
}