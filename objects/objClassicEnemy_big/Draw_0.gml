/// @description Animations
if(global.DEBUG){
	draw_self();
}

if (!isDied){
	animSpd = (animCurSpd/10);

	if(!isGrounded){
		animCurSpd = 0;
		if (rotDelay <= 0){
			animAngle -= 90*Dir;
			rotDelay = rotDelayDef;
		}
		if (!Angery){
			curAnim = EC_b_anim.fall;
		} else {
			curAnim = EC_b_anim.angerFall;
		}	
		
	} else {
		animCurSpd = 2;
		if (!Angery){
			curAnim = EC_b_anim.run;
		} else {
			curAnim = EC_b_anim.angerRun;
		}	
	}

	animFrame += animSpd;
	if (animFrame >= sprite_get_number(curAnim)) {
		animFrame = 0;
	}
	if (animFrame < 0){
		animFrame = sprite_get_number(curAnim);
	}
} else {
	animAngle += animMAngle;
}
if(!global.DEBUG){
	draw_sprite_ext(curAnim, floor(animFrame), x, y, 1*Dir, 1, animAngle, color, 1);
}