/// @description Movement

hitShowDelay = max(hitShowDelay - 1, 0);

if (hitShowDelay > 0){
	color = c_red;
} else {
	color = c_white;
}

if (HP <= 0){
	isDied = true;
}

if (!isDied){
	isGrounded = place_meeting(x,y+1,objBlock);
	
	if (isGrounded && gBegin){
		GSnd = true;
		gBegin = false;
	}
	
	if (isGrounded && !GSnd && !gBegin){
		audio_play_sound(sfx.bigEnGrounded, 9, false);
		scrScreenShake(5, 5);
		GSnd = true;
	} else if (!isGrounded){
		GSnd = false;
	}
	
	XMove = XSpd*Dir;
	YMove += YGrav;

	if (place_meeting(x+XMove, y, objBlock)){
		while(!place_meeting(x+sign(XMove), y, objBlock)){
			x += sign(XMove);
		}
		XMove = 0;
		Dir *= -1;
	}
	x += XMove;

	if (place_meeting(x, y+YMove, objBlock)){
		while(!place_meeting(x, y+sign(YMove), objBlock)){
			y += sign(YMove);
		}
		YMove = 0;
	}
	y += YMove;

} else if (!ded){
	audio_play_sound(sfx.bigEnDeath, 8, false);
	animMAngle = random_range(-10, 10)
	YMove = -random_range(2, 4);
	
	ded = true;
} else {
	YMove += YGrav;
	y += YMove;
	x += XMove*MDir;
}

if (place_meeting(x, y, objPlayerKillTrig) && !isDied){
	if (!Angery){
		audio_play_sound(sfx.enAnger, 7, false);
		XSpd *=2;
	}
	Angery = true;
	y = -17-(17/2);
}

var bp = objBullet;
if (instance_exists(bp)){
	if (bp.type == "disc" && !place_meeting(x, y, bp) && isDelayDamaged){
		isDelayDamaged = false;
	}
}


















