/// @description Init

XPos = x;
YPos = y;

MSpeed  = 0;
MSpdAcc = random_range(0.2, 1.25);
MAngle  = 0;
MDamage = 2;

DMG = 5;
type = "";

spr = sprBullet;

isBounced = false;

sprite_index = spr;