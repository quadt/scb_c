{
    "id": "37c797ba-f40f-4550-ba41-948de6f3f786",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBullet",
    "eventList": [
        {
            "id": "892034e6-b423-42c6-9477-b1b8c8263886",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "37c797ba-f40f-4550-ba41-948de6f3f786"
        },
        {
            "id": "c08803af-9d2b-42ce-ae82-692c2519f554",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "37c797ba-f40f-4550-ba41-948de6f3f786"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "f3cdfd0d-6a58-460b-a4bd-771280673393",
    "visible": true
}