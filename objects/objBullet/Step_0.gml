/// @description

image_angle = MAngle;
if (type == "long"){	
	
} else if (type == "short"){
	MSpeed -= MSpdAcc;
	if (MSpeed < -MSpdAcc*3) {instance_destroy()}
} else if (type == "disc"){

}

if (place_meeting(x, y, objBlock)) {instance_destroy()}


if (type != "disc"){
	XPos += lengthdir_x(max(MSpeed, 0), MAngle);
	YPos += lengthdir_y(max(MSpeed, 0), MAngle);
	
	x = XPos;
	y = YPos;
} else {
	var XMove = lengthdir_x(max(MSpeed, 0), MAngle);
	
	if (place_meeting(x+XMove, y, objBlock) && !isBounced){
		while (!place_meeting(x+sign(XMove), y, objBlock)){
			x += sign(XMove)
		}
		XMove = 0;
		MAngle -= 180;
		audio_play_sound(wepSFX.discGunBounce, 3, false);
		isBounced = true;
	} else if (place_meeting(x+XMove, y, objBlock) && isBounced){
		instance_destroy();
	}
	x += XMove;
	
	YPos += lengthdir_y(max(MSpeed, 0), MAngle);
}
