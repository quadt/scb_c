{
    "id": "6dcc951d-ddd5-4f57-9796-e620f8aaf4eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShell",
    "eventList": [
        {
            "id": "859203d5-f911-484c-91db-b769346a8445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6dcc951d-ddd5-4f57-9796-e620f8aaf4eb"
        },
        {
            "id": "59d85013-8356-46d8-8a06-1d6004aaffc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6dcc951d-ddd5-4f57-9796-e620f8aaf4eb"
        },
        {
            "id": "4da11498-bed5-46af-92e9-245c9dd99f40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "6dcc951d-ddd5-4f57-9796-e620f8aaf4eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "49b745ae-424b-4670-be7e-2b012085f518",
    "visible": true
}