/// @description Movement

FSpd = max(FSpd - 0.05, 0);

if (FSpd <= 0.05) {
	FMSize += 0.1
	
	FSize -= FMSize;
	image_xscale = FSize;
	image_yscale = FSize;
	if (FSize <= 0) {instance_destroy()}
}

x += lengthdir_x(FSpd, FAngle);
y += lengthdir_y(FSpd, FAngle);