{
    "id": "ecf9a9c9-f34c-48c8-a280-7b989a03495a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFog",
    "eventList": [
        {
            "id": "c7ae4648-5211-490c-8756-207eaeaf7ac9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ecf9a9c9-f34c-48c8-a280-7b989a03495a"
        },
        {
            "id": "57975ea0-e05a-401e-ace7-d331b3d669b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ecf9a9c9-f34c-48c8-a280-7b989a03495a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "65095fc6-1984-4cf1-87b5-6acd16830671",
    "visible": true
}