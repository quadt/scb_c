{
    "id": "931a919f-c6bc-452d-90bb-fedb5cf4730f",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fntKarArc",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Karmatic Arcade",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "97af59ce-73dd-41fe-9eca-0f2676a1f41b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 314,
                "y": 242
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4722cd1b-2794-4558-82bd-367d8388943f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 44,
                "offset": 0,
                "shift": 16,
                "w": 20,
                "x": 190,
                "y": 242
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "fec15228-fb1f-4447-aa93-89bb52823cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 0,
                "shift": 27,
                "w": 31,
                "x": 234,
                "y": 242
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ddd9f60d-a7a7-41ba-98a8-6177f0784c26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 0,
                "x": 333,
                "y": 242
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a27f4e6b-5fff-4f5a-8902-926399eb37d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 0,
                "x": 335,
                "y": 242
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "022fc88a-5723-4e92-bf3e-cbe8ffdc4377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 0,
                "x": 337,
                "y": 242
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a405f3e4-a3f0-4c93-b5b1-f2298ebaf2e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 0,
                "shift": 27,
                "w": 0,
                "x": 339,
                "y": 242
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5dea1d80-7d97-4736-9edb-f43b0d5ec067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 20,
                "x": 292,
                "y": 242
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b560b527-34a4-429b-8a22-56b71a1607d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 0,
                "x": 341,
                "y": 242
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f4c275b7-f563-487d-a136-d01e39b21e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 0,
                "x": 343,
                "y": 242
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "809eb267-f7f3-4eac-a1e5-88a124f30cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 0,
                "x": 345,
                "y": 242
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4360b054-10c9-465e-bf4c-e836cc23e98e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 0,
                "x": 349,
                "y": 242
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b376f4c0-d0ef-44b9-83c5-a0939b94c6d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 0,
                "shift": 11,
                "w": 0,
                "x": 369,
                "y": 242
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "85e19d02-1ae8-4704-9790-b5c3be59aca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 23,
                "x": 267,
                "y": 242
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7111e99c-e161-4ed2-9a16-f20b40384a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 44,
                "offset": 4,
                "shift": 24,
                "w": 20,
                "x": 212,
                "y": 242
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e746d58e-daa8-498d-a4f8-bdd330bb0cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": 0,
                "shift": 11,
                "w": 0,
                "x": 351,
                "y": 242
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ac403891-7de2-4300-bf22-1ec44c22c54b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 125,
                "y": 146
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0486d08a-6b4b-40fb-be51-945cf014400f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 44,
                "offset": 0,
                "shift": 31,
                "w": 35,
                "x": 2,
                "y": 242
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "924d3a21-464b-4cc2-b152-d4a76c92f86d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 207,
                "y": 146
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "aa119cb0-72f6-4eed-aca1-0abc69ecd6cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 248,
                "y": 146
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2d9425d3-2f46-4840-aebd-e045ce0961c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 289,
                "y": 146
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8c52a760-18b0-4a1b-ad00-dc97c23ec071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 371,
                "y": 146
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "df54eb14-f478-4048-a3ea-f1b5d832a29a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 412,
                "y": 146
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "48bb655a-410c-4560-bb9e-9a6932b56099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b5e00c97-6739-4cfb-84e4-c5cc2e0130b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 43,
                "y": 194
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6ceda317-8a64-4cc0-97dd-57fa58848ee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 84,
                "y": 194
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "343967e3-067a-42d0-8f43-f276ca72411e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 0,
                "shift": 11,
                "w": 0,
                "x": 353,
                "y": 242
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0e394b02-5547-4fca-9031-fae9092d4ce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 0,
                "shift": 11,
                "w": 0,
                "x": 355,
                "y": 242
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7894a024-f0b9-4665-80de-5d65c61aa0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 0,
                "x": 357,
                "y": 242
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3e4a120d-f727-4601-96f1-5a59c426d134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 0,
                "x": 359,
                "y": 242
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d2f638c9-48e5-4bd7-9b37-52599183bda2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 0,
                "x": 361,
                "y": 242
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6bd39f9e-a8f6-4566-8390-88cfb7b0529e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 43,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e10ab74b-0fe6-47c2-988f-efd17b78b479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 289,
                "y": 98
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "26f1912d-8dec-4bea-8e36-df6e9f83044b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 248,
                "y": 98
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5be8265b-78cb-4717-aefb-6a4ef71a47d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "33ad7f79-ab38-494f-8127-284c52373f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 330,
                "y": 98
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cd7bb837-2ae2-4d99-8ef4-5615697c8d53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5f751e8f-40da-4179-a4d7-c401a4f2b4c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "50c49e67-7110-411c-a297-b86c7cf870ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e000eca3-c5c3-4f2c-8072-3d6d9a748766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2acd4cf2-cde9-481a-8e91-9e68d8574c58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 289,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1d1777d9-908f-4bb5-a006-84069f30e7e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 44,
                "offset": 0,
                "shift": 16,
                "w": 20,
                "x": 168,
                "y": 242
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6a024176-ebfe-49b7-836c-2e94725ed3bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "312e0637-980f-4998-85d1-cb1ebc627c05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6e32cd8b-587e-432b-8480-a9716e0d80d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 44,
                "offset": 0,
                "shift": 31,
                "w": 35,
                "x": 39,
                "y": 242
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "24d94c4a-bff2-4f4c-8f17-63a37291ef31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e72a1901-67f0-4bfc-9566-3cdfbd16745f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 453,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "527ec4a6-39d0-4638-b3a6-00c0d38eeed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "64d903fa-cff5-4d38-a7b7-d86fe692baa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 43,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "bf86f70a-3560-439b-b930-03497da6da26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 84,
                "y": 50
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6b619e1a-807d-4e67-8a0c-31aa347ea146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 125,
                "y": 50
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0ca6bcf9-e9f2-437b-8163-f57ab8af04bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 166,
                "y": 50
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0a225e5c-a6bf-486f-95ab-730c455846a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 44,
                "offset": 0,
                "shift": 31,
                "w": 35,
                "x": 441,
                "y": 194
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1f6e0e11-dc73-4b6d-b933-946d24f753b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 207,
                "y": 50
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5d38b2dd-53e5-4ca5-8ee5-5e837d6cd9ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 248,
                "y": 50
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e7d76d15-d740-4e47-8947-5b79d0b1f7bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 289,
                "y": 50
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f15b0449-bea8-4a6a-bc6a-bc05e3fef88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 330,
                "y": 50
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "464ef0b0-c24e-4392-a4c7-d2ec8e1f46c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 44,
                "offset": 0,
                "shift": 31,
                "w": 35,
                "x": 76,
                "y": 242
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5481e858-dc2b-4a79-8cd6-bbca9c406552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 371,
                "y": 50
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d4ac0bed-ed6d-4b8e-9933-4ed40929e22a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 0,
                "shift": 11,
                "w": 0,
                "x": 365,
                "y": 242
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "24a9663e-91ce-44b3-8f18-e9cd9b031735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 0,
                "shift": 11,
                "w": 0,
                "x": 367,
                "y": 242
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e64b3c2c-8309-46f2-9e2b-d409c16aa06d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 0,
                "shift": 11,
                "w": 0,
                "x": 327,
                "y": 242
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c561ba28-5800-4279-80d6-605fda4cbb45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 32,
                "offset": 0,
                "shift": 27,
                "w": 31,
                "x": 113,
                "y": 242
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0525bb86-23b3-40ed-b929-39580693cf48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f2cd9b58-9ecb-43a2-ad2c-ac497e70d99f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 0,
                "x": 347,
                "y": 242
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4e513565-affe-4c10-a957-5742824c62a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 84,
                "y": 98
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c08fc555-8a43-47c7-82af-9db230df6117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 125,
                "y": 98
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "96ce5308-5703-45fe-a98d-a4001ab7c859",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 166,
                "y": 98
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ee0ef621-efe2-4070-822b-7ca28c54a386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 207,
                "y": 98
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7de1287d-7574-4a6a-bdd8-7888b3a27b56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "68a1cd80-b89c-40a9-9f37-e5d88630694d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 371,
                "y": 98
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "592a625a-2621-45c8-b095-60c653c7254f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 43,
                "y": 98
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3e878d7e-6523-4715-97c7-67ed6f8ec9f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 453,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7d04642e-a245-4479-95c9-32444937eca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 44,
                "offset": 0,
                "shift": 16,
                "w": 20,
                "x": 146,
                "y": 242
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "824290b3-b67b-450e-b1b4-be6882c00927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 412,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "37c014ed-95c7-4100-b279-f931248cf718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "eb243a4b-c845-4a60-b99c-a4e3fb8a859d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 44,
                "offset": 0,
                "shift": 31,
                "w": 35,
                "x": 367,
                "y": 194
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a5246180-b4a8-48de-8df7-f8ee6520033d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 248,
                "y": 194
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "023ad2f5-70d8-442e-99b8-4ea5ea2654ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 207,
                "y": 194
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "43a8761f-d4d0-4c11-8928-6ac330999f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 166,
                "y": 194
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "950c9a15-a502-4cbf-a0f4-26cac8689e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 125,
                "y": 194
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "933aeca0-a647-4fe0-b04f-40ba68f87a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 84,
                "y": 146
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ab884b1f-5d79-4760-bebe-f866f4e56c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 412,
                "y": 98
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d42b34ff-f3f8-435d-aece-ecd1ed090c10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 289,
                "y": 194
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d7967941-28e1-4d27-a93a-c25fc087a078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 44,
                "offset": 0,
                "shift": 31,
                "w": 35,
                "x": 404,
                "y": 194
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d952318b-b3e7-4f75-80cd-927f5f606c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 330,
                "y": 146
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "21f1cff4-dd55-432e-a27b-d036bcd0de4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 453,
                "y": 98
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1cd39ffd-9a43-4c89-94fa-cd249c5ae843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a6c5810a-e30d-4bc1-976b-6d8c0ee44f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 166,
                "y": 146
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5c9afd51-559b-4e8d-8033-0102ec13d75d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 44,
                "offset": 0,
                "shift": 31,
                "w": 35,
                "x": 330,
                "y": 194
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0f6846c4-9d8f-48c7-a52b-ddf4859d9c70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 44,
                "offset": 0,
                "shift": 35,
                "w": 39,
                "x": 453,
                "y": 146
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8456779f-14f1-47d3-91ef-879249802c45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 0,
                "x": 331,
                "y": 242
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d5094966-6f51-43c8-9706-d97d31bc4c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 0,
                "x": 329,
                "y": 242
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e3696883-1fad-4001-b2ee-536db9cac286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 0,
                "x": 363,
                "y": 242
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ebb86a2a-edd7-4937-99dd-f68025adb67d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 0,
                "x": 371,
                "y": 242
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}