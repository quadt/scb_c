{
    "id": "dee0d931-0ad8-45c3-ad48-2ad02ae9d659",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f98e7a53-c24d-4252-a1b5-8040bfbc263e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dee0d931-0ad8-45c3-ad48-2ad02ae9d659",
            "compositeImage": {
                "id": "d114787a-c110-448c-903e-e5135298b1dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98e7a53-c24d-4252-a1b5-8040bfbc263e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff1e18fd-fb18-4fd8-a16e-47efad190ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98e7a53-c24d-4252-a1b5-8040bfbc263e",
                    "LayerId": "888339a3-312e-46d4-82d9-3b5cc9cc9709"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "888339a3-312e-46d4-82d9-3b5cc9cc9709",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dee0d931-0ad8-45c3-ad48-2ad02ae9d659",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}