{
    "id": "a14c2e5f-4344-4dce-9718-62f11b20e39a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBigEnSpawner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2169d472-a843-448d-bb27-8b8a263f95c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a14c2e5f-4344-4dce-9718-62f11b20e39a",
            "compositeImage": {
                "id": "9f6fb6b7-8c3f-46bf-8381-0a38b90e228b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2169d472-a843-448d-bb27-8b8a263f95c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c7fe850-6a9e-4ee3-aa07-e0336b47835b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2169d472-a843-448d-bb27-8b8a263f95c8",
                    "LayerId": "e7784467-e0e7-42ea-ac80-f40e43e0f1c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "e7784467-e0e7-42ea-ac80-f40e43e0f1c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a14c2e5f-4344-4dce-9718-62f11b20e39a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 9
}