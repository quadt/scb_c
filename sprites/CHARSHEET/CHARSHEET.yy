{
    "id": "23e780f6-8a9a-4226-9332-b279062a2fd0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "CHARSHEET",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 494,
    "bbox_left": 0,
    "bbox_right": 487,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3000a846-d6f8-45d2-8d3e-ccfdf46428c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23e780f6-8a9a-4226-9332-b279062a2fd0",
            "compositeImage": {
                "id": "486928b7-9646-4388-8cbf-77073af128c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3000a846-d6f8-45d2-8d3e-ccfdf46428c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d92053b3-1400-4f17-b78f-461e00a77e68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3000a846-d6f8-45d2-8d3e-ccfdf46428c9",
                    "LayerId": "01ea8d99-6097-48e0-b6d0-76f0b8bb8d7a"
                },
                {
                    "id": "33dce6d3-e528-4218-9b99-36f5ccafb174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3000a846-d6f8-45d2-8d3e-ccfdf46428c9",
                    "LayerId": "6ffae9f9-df6e-4415-8be5-7ffb7dcf76c0"
                },
                {
                    "id": "7fcad511-3963-4662-bee1-e8a1c30e25ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3000a846-d6f8-45d2-8d3e-ccfdf46428c9",
                    "LayerId": "f44fb595-f18f-4dc6-8e1c-0abfb4d29969"
                },
                {
                    "id": "6e97d6c9-f971-4940-965b-ea231bdae471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3000a846-d6f8-45d2-8d3e-ccfdf46428c9",
                    "LayerId": "9e263d86-88be-48db-a8fb-15b0cdf68a9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "9e263d86-88be-48db-a8fb-15b0cdf68a9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23e780f6-8a9a-4226-9332-b279062a2fd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Characters",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6ffae9f9-df6e-4415-8be5-7ffb7dcf76c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23e780f6-8a9a-4226-9332-b279062a2fd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "References",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f44fb595-f18f-4dc6-8e1c-0abfb4d29969",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23e780f6-8a9a-4226-9332-b279062a2fd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "BackRefs",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "01ea8d99-6097-48e0-b6d0-76f0b8bb8d7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23e780f6-8a9a-4226-9332-b279062a2fd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Grid",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 250,
    "yorig": 250
}