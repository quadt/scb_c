{
    "id": "89a3a88f-f5b2-4204-b38e-02b7499ab9c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprClassic_small_HitBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "95e7fe8a-bb25-461e-9c7c-31beffb902e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89a3a88f-f5b2-4204-b38e-02b7499ab9c2",
            "compositeImage": {
                "id": "7ed1ddb7-020b-4599-af22-fa5be588ffba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95e7fe8a-bb25-461e-9c7c-31beffb902e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df97de2f-155b-40a1-afc4-5db77f70ab42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95e7fe8a-bb25-461e-9c7c-31beffb902e2",
                    "LayerId": "163654df-2035-4b98-8953-138bb3c411b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "163654df-2035-4b98-8953-138bb3c411b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89a3a88f-f5b2-4204-b38e-02b7499ab9c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 5
}