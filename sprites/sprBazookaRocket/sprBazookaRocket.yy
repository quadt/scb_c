{
    "id": "1b911f24-c452-41c1-b40b-868cd35a6645",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBazookaRocket",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c0fb5efd-c664-4f11-b575-7fd955992f1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b911f24-c452-41c1-b40b-868cd35a6645",
            "compositeImage": {
                "id": "721d477f-c9cb-4e26-9796-0504a43cb755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0fb5efd-c664-4f11-b575-7fd955992f1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b60575a8-ec3e-443d-aa24-b33faef1a8af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0fb5efd-c664-4f11-b575-7fd955992f1c",
                    "LayerId": "64bf9481-9027-41af-8d79-a872b6371572"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "64bf9481-9027-41af-8d79-a872b6371572",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b911f24-c452-41c1-b40b-868cd35a6645",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 2
}