{
    "id": "4ef313eb-c92d-4a60-9160-40654a710930",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "fae5ea8c-47a9-478c-b514-304aa54c5098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef313eb-c92d-4a60-9160-40654a710930",
            "compositeImage": {
                "id": "4bbdd3b2-e50f-4853-a80d-ad2249adda27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fae5ea8c-47a9-478c-b514-304aa54c5098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c525e0-c297-4424-ac73-0f791e2e6080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fae5ea8c-47a9-478c-b514-304aa54c5098",
                    "LayerId": "9ec7c883-7b66-429a-bc35-2a1ab88c6606"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "9ec7c883-7b66-429a-bc35-2a1ab88c6606",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ef313eb-c92d-4a60-9160-40654a710930",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}