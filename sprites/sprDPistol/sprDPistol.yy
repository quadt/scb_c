{
    "id": "df6861a6-d059-4c96-9f1e-95c1b2ef36f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDPistol",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d222c3b9-57e0-47fc-be7a-df1d3451e520",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df6861a6-d059-4c96-9f1e-95c1b2ef36f0",
            "compositeImage": {
                "id": "8bf2b962-0e75-4a83-b39d-30de3c3b65b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d222c3b9-57e0-47fc-be7a-df1d3451e520",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65e9050d-453b-4f4c-bfe9-a88122568030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d222c3b9-57e0-47fc-be7a-df1d3451e520",
                    "LayerId": "bbef39e0-77d4-477b-99d4-444f5ab72896"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "bbef39e0-77d4-477b-99d4-444f5ab72896",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df6861a6-d059-4c96-9f1e-95c1b2ef36f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 1
}