{
    "id": "127acae0-fd69-4fdd-914c-428188ef584e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sheet11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 281,
    "bbox_left": 0,
    "bbox_right": 283,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "cbf1cc31-2d59-46a7-84be-f3513d170e12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "127acae0-fd69-4fdd-914c-428188ef584e",
            "compositeImage": {
                "id": "3236c970-ee1f-4518-9f35-5b6492a5a1bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf1cc31-2d59-46a7-84be-f3513d170e12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15af896e-b198-4af1-8db1-1de12dbb1cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf1cc31-2d59-46a7-84be-f3513d170e12",
                    "LayerId": "9f394247-353b-4f70-8118-973cd5ef8159"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 282,
    "layers": [
        {
            "id": "9f394247-353b-4f70-8118-973cd5ef8159",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "127acae0-fd69-4fdd-914c-428188ef584e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 284,
    "xorig": 57,
    "yorig": 85
}