{
    "id": "71a9f925-d5f7-4788-b7f3-d70233cf6a14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCrate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "97c2ca2d-404b-4da5-8dc0-e58d39521eeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71a9f925-d5f7-4788-b7f3-d70233cf6a14",
            "compositeImage": {
                "id": "a2b43935-8dfd-4690-9ca5-0343018bb6b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97c2ca2d-404b-4da5-8dc0-e58d39521eeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d536379f-776a-46ed-9e2b-5b0196c435c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c2ca2d-404b-4da5-8dc0-e58d39521eeb",
                    "LayerId": "22c82de1-207f-440e-b91c-d3a0031ed1f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "22c82de1-207f-440e-b91c-d3a0031ed1f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71a9f925-d5f7-4788-b7f3-d70233cf6a14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}