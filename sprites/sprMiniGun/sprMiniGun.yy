{
    "id": "0740b4af-2680-4236-94b6-e08a7eb4f6e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMiniGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "60085e63-c155-4acd-b9a0-7056403b5d97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0740b4af-2680-4236-94b6-e08a7eb4f6e7",
            "compositeImage": {
                "id": "78085259-675b-4bb5-bd63-a6560359c126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60085e63-c155-4acd-b9a0-7056403b5d97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1400772-c596-49c2-bbd3-68c5ba8e8552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60085e63-c155-4acd-b9a0-7056403b5d97",
                    "LayerId": "003dce89-472e-4202-8208-98b40a30a37b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "003dce89-472e-4202-8208-98b40a30a37b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0740b4af-2680-4236-94b6-e08a7eb4f6e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 2,
    "yorig": 4
}