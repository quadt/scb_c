{
    "id": "cda1158a-eccd-4cbf-b00d-5b44570405d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprJumpBlockingTrig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6c7b73bb-037c-4df8-b30b-064e5fd91f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cda1158a-eccd-4cbf-b00d-5b44570405d7",
            "compositeImage": {
                "id": "a0cbb117-915e-46ad-b8ed-577035f86fc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c7b73bb-037c-4df8-b30b-064e5fd91f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13c7be5-7af6-4abd-9e0b-9df361cca038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c7b73bb-037c-4df8-b30b-064e5fd91f3e",
                    "LayerId": "60c0b558-eca6-4bee-8124-ea0add1fe46e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "60c0b558-eca6-4bee-8124-ea0add1fe46e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cda1158a-eccd-4cbf-b00d-5b44570405d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}