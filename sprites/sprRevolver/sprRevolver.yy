{
    "id": "52927353-d1f5-481a-9035-c1b3f6ecd900",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRevolver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d6a28c24-8220-459c-ba5b-c878d0a95fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52927353-d1f5-481a-9035-c1b3f6ecd900",
            "compositeImage": {
                "id": "8dc7a0d9-c1db-47f4-986f-e4adea65c3ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6a28c24-8220-459c-ba5b-c878d0a95fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bec35d1a-15da-4efd-97bc-de60fd98fa05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6a28c24-8220-459c-ba5b-c878d0a95fc7",
                    "LayerId": "f1f5ae37-feb4-421a-a5cf-a2224716f6e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "f1f5ae37-feb4-421a-a5cf-a2224716f6e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52927353-d1f5-481a-9035-c1b3f6ecd900",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 2,
    "yorig": 2
}