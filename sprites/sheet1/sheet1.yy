{
    "id": "35cf5b01-85d6-4b92-8e0a-bef543fc47d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sheet1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 237,
    "bbox_left": 0,
    "bbox_right": 271,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c8018aae-879c-4ae2-a583-679f30bf82e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35cf5b01-85d6-4b92-8e0a-bef543fc47d3",
            "compositeImage": {
                "id": "4ef8403a-dd19-4388-90ea-dcf7afbfdacd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8018aae-879c-4ae2-a583-679f30bf82e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae5340e7-2708-49f2-9812-998d7f90b0ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8018aae-879c-4ae2-a583-679f30bf82e6",
                    "LayerId": "c7f27e78-043c-4901-b131-6a516b36e0e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 238,
    "layers": [
        {
            "id": "c7f27e78-043c-4901-b131-6a516b36e0e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35cf5b01-85d6-4b92-8e0a-bef543fc47d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 272,
    "xorig": 148,
    "yorig": 90
}