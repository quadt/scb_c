{
    "id": "052470b6-2064-4419-963c-bacdfc1c7fc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPistol",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7569e84d-e496-4003-b446-d489dcf0eae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "052470b6-2064-4419-963c-bacdfc1c7fc9",
            "compositeImage": {
                "id": "6dd03a98-0d64-4897-9432-5123158598ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7569e84d-e496-4003-b446-d489dcf0eae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c41d493a-0f1e-4f1e-8bd9-0a95be4f0955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7569e84d-e496-4003-b446-d489dcf0eae6",
                    "LayerId": "37810d91-ea4e-42f7-b044-94fdb45e52ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "37810d91-ea4e-42f7-b044-94fdb45e52ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "052470b6-2064-4419-963c-bacdfc1c7fc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 1
}