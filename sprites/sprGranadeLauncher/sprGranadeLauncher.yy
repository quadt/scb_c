{
    "id": "d839b1b4-0a34-4fbc-8cdb-29cb5ecc831a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGranadeLauncher",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6a2da27f-5b0a-4dc8-a0a9-f50d259b3832",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d839b1b4-0a34-4fbc-8cdb-29cb5ecc831a",
            "compositeImage": {
                "id": "644b0f85-4cad-4845-b63c-386f7a573f02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a2da27f-5b0a-4dc8-a0a9-f50d259b3832",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c620b505-94aa-4d19-9502-a747f67f4f52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a2da27f-5b0a-4dc8-a0a9-f50d259b3832",
                    "LayerId": "0954c018-dca6-455b-bc7d-c9748774774d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "0954c018-dca6-455b-bc7d-c9748774774d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d839b1b4-0a34-4fbc-8cdb-29cb5ecc831a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 2,
    "yorig": 3
}