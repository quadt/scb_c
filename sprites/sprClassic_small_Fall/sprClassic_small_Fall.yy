{
    "id": "e9cd6ab0-df1c-4479-94ef-87bc0b4d1d41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprClassic_small_Fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c457b098-ab7a-4dca-ad60-e964728c96e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9cd6ab0-df1c-4479-94ef-87bc0b4d1d41",
            "compositeImage": {
                "id": "4a27093a-9e06-4bc8-8747-6c87869531e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c457b098-ab7a-4dca-ad60-e964728c96e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9705cabe-0a11-4ea3-acda-aacb2b6a7099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c457b098-ab7a-4dca-ad60-e964728c96e8",
                    "LayerId": "af21a421-c8e7-4396-b263-1ec5c2aa168f"
                }
            ]
        },
        {
            "id": "be3ec811-b0e7-40c2-8421-65fdfcd76144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9cd6ab0-df1c-4479-94ef-87bc0b4d1d41",
            "compositeImage": {
                "id": "2d6190e5-4ae9-41b6-9cea-3fc49296897b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3ec811-b0e7-40c2-8421-65fdfcd76144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aa5808b-80ed-443c-a757-0ff9e1da45ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3ec811-b0e7-40c2-8421-65fdfcd76144",
                    "LayerId": "af21a421-c8e7-4396-b263-1ec5c2aa168f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "af21a421-c8e7-4396-b263-1ec5c2aa168f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9cd6ab0-df1c-4479-94ef-87bc0b4d1d41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 5
}