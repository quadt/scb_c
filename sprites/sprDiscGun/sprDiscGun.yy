{
    "id": "dd5fcd23-e759-45b9-8976-e53b7d05adb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDiscGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bc4f664e-79cf-4110-8f9b-400b48f7cb2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd5fcd23-e759-45b9-8976-e53b7d05adb4",
            "compositeImage": {
                "id": "d64b42e3-85c0-4041-b861-42a7fe5b2cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc4f664e-79cf-4110-8f9b-400b48f7cb2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ffb750d-3675-43cc-827b-f30c5f2beb3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc4f664e-79cf-4110-8f9b-400b48f7cb2c",
                    "LayerId": "e6e72aaa-f8ce-40e7-93bb-be452c0c4b02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "e6e72aaa-f8ce-40e7-93bb-be452c0c4b02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd5fcd23-e759-45b9-8976-e53b7d05adb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 1,
    "yorig": 2
}