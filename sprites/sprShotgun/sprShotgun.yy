{
    "id": "18b74a20-4d0e-429b-a954-205e64d09c39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprShotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6f88ba1d-5df4-46ae-a736-40e82300501e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18b74a20-4d0e-429b-a954-205e64d09c39",
            "compositeImage": {
                "id": "06f1763f-1da2-4fcb-bc17-1b2d3bab7212",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f88ba1d-5df4-46ae-a736-40e82300501e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e67b063-e6f7-4b19-9360-a755d34fad74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f88ba1d-5df4-46ae-a736-40e82300501e",
                    "LayerId": "0f905ce7-0a8c-4299-9649-8e9cb19721d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "0f905ce7-0a8c-4299-9649-8e9cb19721d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18b74a20-4d0e-429b-a954-205e64d09c39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 3,
    "yorig": 2
}