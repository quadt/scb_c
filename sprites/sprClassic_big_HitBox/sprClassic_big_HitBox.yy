{
    "id": "31b8757f-cc51-43ee-94e9-a4fb698f82be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprClassic_big_HitBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "93014b13-c9b8-4b98-b5ff-e021df60bf2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31b8757f-cc51-43ee-94e9-a4fb698f82be",
            "compositeImage": {
                "id": "2fa44555-e32b-4836-aac2-2f9f4f9ba8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93014b13-c9b8-4b98-b5ff-e021df60bf2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b33626e0-eb65-40ca-9143-ac215aa74c16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93014b13-c9b8-4b98-b5ff-e021df60bf2f",
                    "LayerId": "ef993397-1d3c-46ab-9119-d7ed3f732054"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "ef993397-1d3c-46ab-9119-d7ed3f732054",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31b8757f-cc51-43ee-94e9-a4fb698f82be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 9
}