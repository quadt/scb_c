{
    "id": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprClassic_big_Run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "007e605a-19e4-4c8b-b0f1-b83b3a92a743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "b86c581e-42a6-4094-876b-9d57ab1a0a72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "007e605a-19e4-4c8b-b0f1-b83b3a92a743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85af4160-cc5d-42b8-9f0e-fae13e5b3b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "007e605a-19e4-4c8b-b0f1-b83b3a92a743",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "96778c2f-fc7a-4e14-9a75-61423559a9b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "6e251471-5111-4be6-93fc-c71967c3b176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96778c2f-fc7a-4e14-9a75-61423559a9b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "011e829e-ea2b-4794-a9aa-9e7db66734c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96778c2f-fc7a-4e14-9a75-61423559a9b8",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "424fd965-1431-437e-9cfa-3561cd1e4fa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "75333d39-06e6-4e84-b173-c58e8676d248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "424fd965-1431-437e-9cfa-3561cd1e4fa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a51ca725-1fb9-4671-bdf4-2f8925793a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "424fd965-1431-437e-9cfa-3561cd1e4fa9",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "cac54e8c-c9bb-4795-93a1-df6da9b52c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "ba983e88-9e56-462a-9886-52eb931facb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cac54e8c-c9bb-4795-93a1-df6da9b52c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1169e619-63e0-44dc-8bb5-c29ebb75230e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cac54e8c-c9bb-4795-93a1-df6da9b52c16",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "db00283a-66e8-42ec-882e-19fe8f92d0e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "f32d6a80-6f00-4702-beab-a1e968f0e976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db00283a-66e8-42ec-882e-19fe8f92d0e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c62de617-e438-46ba-9596-872e7b4e707d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db00283a-66e8-42ec-882e-19fe8f92d0e7",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "faf6e6e2-6531-45d9-9a6a-4d8c40e59116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "4b1e270f-2558-4045-83dc-c2e3b967976f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faf6e6e2-6531-45d9-9a6a-4d8c40e59116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "942c065f-b16b-462f-a0b1-bd39e8d7b349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faf6e6e2-6531-45d9-9a6a-4d8c40e59116",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "7e2fed94-9a34-4109-93ca-68def07883e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "670647b2-630e-475b-ae9a-ca9c4cc26604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e2fed94-9a34-4109-93ca-68def07883e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdee22c0-c2f2-4c20-a621-1761b3a755b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e2fed94-9a34-4109-93ca-68def07883e1",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "a3b405a2-19dc-424b-8a40-bc40f68040f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "78c020e8-bec7-49ba-a89a-23fd8824b78d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3b405a2-19dc-424b-8a40-bc40f68040f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "343f9d56-9bc8-46b8-8a12-806a7c3ae607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b405a2-19dc-424b-8a40-bc40f68040f8",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "8b3088dc-53b5-4db2-bf25-73a9387adf2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "3395b6b8-19bb-41c4-9385-64d7703096f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3088dc-53b5-4db2-bf25-73a9387adf2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb95eca0-9b01-447f-ac8e-7952dccb6610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3088dc-53b5-4db2-bf25-73a9387adf2a",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "43bcc580-1ab5-4e6d-853d-7c70f7140e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "a0ef8a93-8ebf-4c56-a6b5-c0c1f4d7f77f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43bcc580-1ab5-4e6d-853d-7c70f7140e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dfab533-4b6d-4634-8fc0-7b377db3fb26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43bcc580-1ab5-4e6d-853d-7c70f7140e9e",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        },
        {
            "id": "4caa783f-9ac8-4c25-b4be-28288a674238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "compositeImage": {
                "id": "34df968f-1483-4449-ace4-5d0918c63a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4caa783f-9ac8-4c25-b4be-28288a674238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc16f978-4b73-43cd-a3bb-cdfa538592de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4caa783f-9ac8-4c25-b4be-28288a674238",
                    "LayerId": "34b89168-c680-4f0f-9235-c9bb927d2983"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "34b89168-c680-4f0f-9235-c9bb927d2983",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0a325c6-aeef-4168-bdee-49d6c66e1a79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}