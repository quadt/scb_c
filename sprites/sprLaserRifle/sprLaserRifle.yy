{
    "id": "1a06cc9f-cff2-491a-b486-e8d56a15faf0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLaserRifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "736d7615-58b9-4807-848c-f0ef153481ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a06cc9f-cff2-491a-b486-e8d56a15faf0",
            "compositeImage": {
                "id": "99f07ba0-8836-4085-9604-b4f7ce64577c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "736d7615-58b9-4807-848c-f0ef153481ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4525d7d5-ef43-42d8-abd4-6a3f41d4f990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "736d7615-58b9-4807-848c-f0ef153481ee",
                    "LayerId": "eba975d8-27d1-46e2-85ef-122dce7e0b41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "eba975d8-27d1-46e2-85ef-122dce7e0b41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a06cc9f-cff2-491a-b486-e8d56a15faf0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 2,
    "yorig": 3
}