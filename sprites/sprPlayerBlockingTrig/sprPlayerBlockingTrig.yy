{
    "id": "ab90d223-3acb-4f11-9d3e-19ba022e6084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerBlockingTrig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "42a31552-2117-4a3e-a158-e7d1a579bdad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab90d223-3acb-4f11-9d3e-19ba022e6084",
            "compositeImage": {
                "id": "8e1ef8c9-d38c-4ebc-88e6-e686450b4e57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a31552-2117-4a3e-a158-e7d1a579bdad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "964b7fa6-7d5b-4b85-b4b3-94ca97076820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a31552-2117-4a3e-a158-e7d1a579bdad",
                    "LayerId": "007fd958-6b04-443b-b1cf-7c9a7c5653f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "007fd958-6b04-443b-b1cf-7c9a7c5653f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab90d223-3acb-4f11-9d3e-19ba022e6084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}