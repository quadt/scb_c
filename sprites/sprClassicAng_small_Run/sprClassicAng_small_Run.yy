{
    "id": "c3356ea9-1620-4c53-8b87-d2485cb2b5b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprClassicAng_small_Run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "05b95e1e-dd09-4227-b66c-59ca80b21f82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3356ea9-1620-4c53-8b87-d2485cb2b5b5",
            "compositeImage": {
                "id": "e7567baf-9af0-4f7a-afba-52ff68e03ba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05b95e1e-dd09-4227-b66c-59ca80b21f82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9fa5925-a7f2-4c3f-aa62-fa0e4b83e8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05b95e1e-dd09-4227-b66c-59ca80b21f82",
                    "LayerId": "bdd53b79-2dd7-485c-86fd-d40c2236f171"
                }
            ]
        },
        {
            "id": "fd6cbe99-0467-4a9c-ad03-d22d295c5022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3356ea9-1620-4c53-8b87-d2485cb2b5b5",
            "compositeImage": {
                "id": "b681d466-5ab2-4db2-8966-18f06e80a002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd6cbe99-0467-4a9c-ad03-d22d295c5022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a4df750-9eb7-45d9-9169-de29eabf6751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd6cbe99-0467-4a9c-ad03-d22d295c5022",
                    "LayerId": "bdd53b79-2dd7-485c-86fd-d40c2236f171"
                }
            ]
        },
        {
            "id": "e03aeeba-d985-4e9a-8719-67a226d4a273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3356ea9-1620-4c53-8b87-d2485cb2b5b5",
            "compositeImage": {
                "id": "d9444cb7-0110-4a3a-bd67-b64c990fc400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e03aeeba-d985-4e9a-8719-67a226d4a273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16e507da-7b71-4a7c-b7d4-91527a8784e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e03aeeba-d985-4e9a-8719-67a226d4a273",
                    "LayerId": "bdd53b79-2dd7-485c-86fd-d40c2236f171"
                }
            ]
        },
        {
            "id": "cf95d294-2fbd-483a-ad80-2c36c3eb25b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3356ea9-1620-4c53-8b87-d2485cb2b5b5",
            "compositeImage": {
                "id": "2f682099-4ce9-45a4-bb24-123b56bcfc67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf95d294-2fbd-483a-ad80-2c36c3eb25b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc8e5c80-be34-4ac8-b3eb-016edae96a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf95d294-2fbd-483a-ad80-2c36c3eb25b3",
                    "LayerId": "bdd53b79-2dd7-485c-86fd-d40c2236f171"
                }
            ]
        },
        {
            "id": "84994a62-b34f-47e3-bf25-1fd08c5ae1f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3356ea9-1620-4c53-8b87-d2485cb2b5b5",
            "compositeImage": {
                "id": "efebf28a-309b-47b3-92f1-c36d50b6aa8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84994a62-b34f-47e3-bf25-1fd08c5ae1f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de1e91f6-bac5-4e25-8c9b-148e735b0c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84994a62-b34f-47e3-bf25-1fd08c5ae1f8",
                    "LayerId": "bdd53b79-2dd7-485c-86fd-d40c2236f171"
                }
            ]
        },
        {
            "id": "208e7813-1875-47e1-9cb3-094d904f7d1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3356ea9-1620-4c53-8b87-d2485cb2b5b5",
            "compositeImage": {
                "id": "13373189-020d-44a4-910e-a98203f36ac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208e7813-1875-47e1-9cb3-094d904f7d1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf504260-e8cf-45ff-804b-d6f8f74b6c12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208e7813-1875-47e1-9cb3-094d904f7d1d",
                    "LayerId": "bdd53b79-2dd7-485c-86fd-d40c2236f171"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "bdd53b79-2dd7-485c-86fd-d40c2236f171",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3356ea9-1620-4c53-8b87-d2485cb2b5b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 4
}