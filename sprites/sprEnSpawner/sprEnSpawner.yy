{
    "id": "2b9e5b9c-251e-4915-89d3-765403b24560",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprEnSpawner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a95bf39c-4024-410b-b76a-845c83628151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b9e5b9c-251e-4915-89d3-765403b24560",
            "compositeImage": {
                "id": "85888a7f-149c-4d7a-86a9-c05338f5c91f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a95bf39c-4024-410b-b76a-845c83628151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c247913-792f-48f2-8fad-e1ddcef17e73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a95bf39c-4024-410b-b76a-845c83628151",
                    "LayerId": "8f6f9cb5-8a58-43bb-a843-fc1db934b63e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "8f6f9cb5-8a58-43bb-a843-fc1db934b63e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b9e5b9c-251e-4915-89d3-765403b24560",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 5
}