{
    "id": "ffaa5cf4-086f-4e4b-a8d0-5c25b64c445d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprKatana",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "78df0443-3a00-49d2-8c39-6af6a1b80378",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffaa5cf4-086f-4e4b-a8d0-5c25b64c445d",
            "compositeImage": {
                "id": "9ea44b90-7aff-4e07-bfa8-1d7352d9af36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78df0443-3a00-49d2-8c39-6af6a1b80378",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f22f3f6f-743d-4d7e-945e-444db570b8dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78df0443-3a00-49d2-8c39-6af6a1b80378",
                    "LayerId": "01d60cc4-4bef-45ba-a021-a1c2f3ac3191"
                }
            ]
        },
        {
            "id": "29f27166-8974-4615-bd69-c52eab0a18c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffaa5cf4-086f-4e4b-a8d0-5c25b64c445d",
            "compositeImage": {
                "id": "cbbcc460-e1d1-463e-91d6-1f3792fe79c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29f27166-8974-4615-bd69-c52eab0a18c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17e2c86-8ca2-4440-81b3-9096c7c10321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29f27166-8974-4615-bd69-c52eab0a18c7",
                    "LayerId": "01d60cc4-4bef-45ba-a021-a1c2f3ac3191"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "01d60cc4-4bef-45ba-a021-a1c2f3ac3191",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffaa5cf4-086f-4e4b-a8d0-5c25b64c445d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 0,
    "yorig": 7
}