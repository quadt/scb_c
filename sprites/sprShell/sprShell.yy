{
    "id": "49b745ae-424b-4670-be7e-2b012085f518",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprShell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "08e57aa8-68da-4291-9d26-06f0d761ac4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49b745ae-424b-4670-be7e-2b012085f518",
            "compositeImage": {
                "id": "daf2fbba-9f69-40eb-b58b-b232cb26b6d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08e57aa8-68da-4291-9d26-06f0d761ac4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601f5016-52e0-4a25-9899-d1f0ec29c95f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08e57aa8-68da-4291-9d26-06f0d761ac4a",
                    "LayerId": "a2d7ef35-8e7e-416f-bf64-0fb3d98e6385"
                }
            ]
        },
        {
            "id": "081c389f-fed0-4967-8025-8acec14e39ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49b745ae-424b-4670-be7e-2b012085f518",
            "compositeImage": {
                "id": "252d946f-35b4-41b1-923e-ed63f48d85ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "081c389f-fed0-4967-8025-8acec14e39ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62beaea5-b6a7-46fa-a48f-65b81b2c7999",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "081c389f-fed0-4967-8025-8acec14e39ec",
                    "LayerId": "a2d7ef35-8e7e-416f-bf64-0fb3d98e6385"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "a2d7ef35-8e7e-416f-bf64-0fb3d98e6385",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49b745ae-424b-4670-be7e-2b012085f518",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 1,
    "yorig": 1
}