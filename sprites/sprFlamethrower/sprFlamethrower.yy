{
    "id": "4beba22b-2c9b-4c0c-bbc0-eee1fbf244e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFlamethrower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ec9f5b43-c580-490d-a5b2-d4372482d6af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4beba22b-2c9b-4c0c-bbc0-eee1fbf244e6",
            "compositeImage": {
                "id": "4e2fcf58-2ffe-4181-b75c-561ea65c34bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec9f5b43-c580-490d-a5b2-d4372482d6af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b58299c-c59b-42a5-8d65-e319596cacb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec9f5b43-c580-490d-a5b2-d4372482d6af",
                    "LayerId": "9422a409-4043-4ec5-9100-7aceb9dbef8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "9422a409-4043-4ec5-9100-7aceb9dbef8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4beba22b-2c9b-4c0c-bbc0-eee1fbf244e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 2,
    "yorig": 2
}