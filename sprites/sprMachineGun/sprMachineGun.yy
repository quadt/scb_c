{
    "id": "4d492d10-8fa9-4947-b836-fbdc16c5d628",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMachineGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "80bf0b00-ae5b-4f9f-860c-8f2326ede472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d492d10-8fa9-4947-b836-fbdc16c5d628",
            "compositeImage": {
                "id": "d396c942-0ea2-46ce-a450-90d66c0f53b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80bf0b00-ae5b-4f9f-860c-8f2326ede472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9614df-e555-4617-8461-f8a5e29357f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80bf0b00-ae5b-4f9f-860c-8f2326ede472",
                    "LayerId": "92d0ca1a-fd8f-4fc3-b20e-a9583b4bca39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "92d0ca1a-fd8f-4fc3-b20e-a9583b4bca39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d492d10-8fa9-4947-b836-fbdc16c5d628",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 2,
    "yorig": 2
}