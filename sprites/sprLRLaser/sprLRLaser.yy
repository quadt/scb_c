{
    "id": "de6ca737-9d36-4189-af50-75df78761d13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLRLaser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "78033ab3-06b1-46d2-8057-92def8bea746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de6ca737-9d36-4189-af50-75df78761d13",
            "compositeImage": {
                "id": "f07e0151-a0df-442e-a705-afb94e10a6d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78033ab3-06b1-46d2-8057-92def8bea746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "007f6482-7512-4a34-8a50-9d7410384e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78033ab3-06b1-46d2-8057-92def8bea746",
                    "LayerId": "e15a6f42-7026-4a23-86c1-1917ada1e46d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "e15a6f42-7026-4a23-86c1-1917ada1e46d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de6ca737-9d36-4189-af50-75df78761d13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 2
}