{
    "id": "2efff811-c1f7-48f1-a406-f52eb75c20f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRevBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "84f5d5ea-5080-44bb-ba70-1a2793bed053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2efff811-c1f7-48f1-a406-f52eb75c20f4",
            "compositeImage": {
                "id": "abe2d47d-3cda-4cf4-9ddc-af63912526e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84f5d5ea-5080-44bb-ba70-1a2793bed053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c80bb9ae-b7fd-4a2f-b098-c3af89b91439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84f5d5ea-5080-44bb-ba70-1a2793bed053",
                    "LayerId": "33b292dd-9fcb-4cd2-948b-ad760f061d8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "33b292dd-9fcb-4cd2-948b-ad760f061d8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2efff811-c1f7-48f1-a406-f52eb75c20f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 2
}