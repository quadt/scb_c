{
    "id": "65095fc6-1984-4cf1-87b5-6acd16830671",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5f6090e2-def7-412d-885b-eafcc6b80300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65095fc6-1984-4cf1-87b5-6acd16830671",
            "compositeImage": {
                "id": "9496c397-b3e5-426c-83fe-ece141ed426a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f6090e2-def7-412d-885b-eafcc6b80300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7634f5c3-1c3f-4be3-83b3-90ae11e7fe85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f6090e2-def7-412d-885b-eafcc6b80300",
                    "LayerId": "14e3a061-a337-486e-b0a8-17530af6c883"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "14e3a061-a337-486e-b0a8-17530af6c883",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65095fc6-1984-4cf1-87b5-6acd16830671",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}