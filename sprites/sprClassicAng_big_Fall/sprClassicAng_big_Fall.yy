{
    "id": "86b2d089-0ae8-45e4-bcad-b565743710b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprClassicAng_big_Fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "80b60059-d947-46a6-83ec-f9c335db5229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b2d089-0ae8-45e4-bcad-b565743710b5",
            "compositeImage": {
                "id": "497014c5-134c-4e07-b231-bb8d7dccc1b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b60059-d947-46a6-83ec-f9c335db5229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca9bc828-e121-4ba0-b9c6-0b95160f4a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b60059-d947-46a6-83ec-f9c335db5229",
                    "LayerId": "82a56994-e4be-41b0-9ff4-d623cd68b97d"
                }
            ]
        },
        {
            "id": "fc50ca68-9e1e-456b-b190-2d67bc834e87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86b2d089-0ae8-45e4-bcad-b565743710b5",
            "compositeImage": {
                "id": "fc0c0c7d-0801-4bda-8906-2d641986c3f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc50ca68-9e1e-456b-b190-2d67bc834e87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "563663fd-fa13-4127-9ee8-9aa662dbfe28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc50ca68-9e1e-456b-b190-2d67bc834e87",
                    "LayerId": "82a56994-e4be-41b0-9ff4-d623cd68b97d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "82a56994-e4be-41b0-9ff4-d623cd68b97d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86b2d089-0ae8-45e4-bcad-b565743710b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 9
}