{
    "id": "f62c6120-b3e9-45b1-9f98-99144a11b470",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCFieldSpawn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "43b00510-5ebd-470b-b9eb-e9f4fd21c3a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f62c6120-b3e9-45b1-9f98-99144a11b470",
            "compositeImage": {
                "id": "4ef295fd-0558-4848-8a47-4a1559247aed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43b00510-5ebd-470b-b9eb-e9f4fd21c3a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf1cc0fb-1c87-483f-8268-918263e1328d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43b00510-5ebd-470b-b9eb-e9f4fd21c3a0",
                    "LayerId": "9dd8b2a0-9357-4bce-bb06-19901b2df7aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "9dd8b2a0-9357-4bce-bb06-19901b2df7aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f62c6120-b3e9-45b1-9f98-99144a11b470",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}