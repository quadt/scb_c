{
    "id": "cd84d939-43ae-49b4-8009-630014ebf612",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayer_Runing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f7fd7d54-0bba-449e-80e4-566be9b3a8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84d939-43ae-49b4-8009-630014ebf612",
            "compositeImage": {
                "id": "e51a8cb5-12a4-48fe-9c81-4b4abbab339c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7fd7d54-0bba-449e-80e4-566be9b3a8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bde7e79-7bac-4661-9584-b648d3f1067e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7fd7d54-0bba-449e-80e4-566be9b3a8d4",
                    "LayerId": "bdf85367-7998-40f7-9cd5-cf446402f422"
                }
            ]
        },
        {
            "id": "0ec48b73-ab9b-4b7d-a9ca-9aeebf48c044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84d939-43ae-49b4-8009-630014ebf612",
            "compositeImage": {
                "id": "06d07f85-6dc8-446d-a933-e2b94f982b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ec48b73-ab9b-4b7d-a9ca-9aeebf48c044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40363af8-597d-4c22-853b-cea45e5d925c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ec48b73-ab9b-4b7d-a9ca-9aeebf48c044",
                    "LayerId": "bdf85367-7998-40f7-9cd5-cf446402f422"
                }
            ]
        },
        {
            "id": "e80c6f68-4330-426d-856a-70858c56bffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84d939-43ae-49b4-8009-630014ebf612",
            "compositeImage": {
                "id": "bd39b35d-5dcc-4f4d-8237-988d650882c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e80c6f68-4330-426d-856a-70858c56bffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50d2bb9e-27e3-47fa-871f-205c029de4c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e80c6f68-4330-426d-856a-70858c56bffb",
                    "LayerId": "bdf85367-7998-40f7-9cd5-cf446402f422"
                }
            ]
        },
        {
            "id": "ed1c268f-81b1-42bc-b967-25c8082d8e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84d939-43ae-49b4-8009-630014ebf612",
            "compositeImage": {
                "id": "2ad5e183-6544-44a0-a50c-d9a538f1e3f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed1c268f-81b1-42bc-b967-25c8082d8e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7564c37f-e882-4b97-9686-c36f4f70b615",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed1c268f-81b1-42bc-b967-25c8082d8e7a",
                    "LayerId": "bdf85367-7998-40f7-9cd5-cf446402f422"
                }
            ]
        },
        {
            "id": "78b27279-b873-43a8-835b-68dff4708d4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84d939-43ae-49b4-8009-630014ebf612",
            "compositeImage": {
                "id": "fddde10d-2e27-4e5f-b725-3eb804a65640",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b27279-b873-43a8-835b-68dff4708d4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fb7d682-b1d3-4ad4-af31-20a0a6195ec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b27279-b873-43a8-835b-68dff4708d4a",
                    "LayerId": "bdf85367-7998-40f7-9cd5-cf446402f422"
                }
            ]
        },
        {
            "id": "a80209af-b70a-4c62-8c54-7f15b51c66c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd84d939-43ae-49b4-8009-630014ebf612",
            "compositeImage": {
                "id": "617cbed2-9486-4de6-ae7e-b8e048d4a44d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80209af-b70a-4c62-8c54-7f15b51c66c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5d00992-9619-4799-8599-0487e69b9fe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80209af-b70a-4c62-8c54-7f15b51c66c9",
                    "LayerId": "bdf85367-7998-40f7-9cd5-cf446402f422"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "bdf85367-7998-40f7-9cd5-cf446402f422",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd84d939-43ae-49b4-8009-630014ebf612",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}