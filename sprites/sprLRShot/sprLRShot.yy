{
    "id": "ba631f13-6eec-4994-a121-02fbce599084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLRShot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ed6c29b0-ab95-4090-93dc-ffe113dbffdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba631f13-6eec-4994-a121-02fbce599084",
            "compositeImage": {
                "id": "b8a618ba-c2d5-4600-ae28-4e7d76974432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed6c29b0-ab95-4090-93dc-ffe113dbffdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcb96eec-e92d-4980-8e47-8d73d6c18089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed6c29b0-ab95-4090-93dc-ffe113dbffdf",
                    "LayerId": "a9526cbf-d916-430b-a456-2d740f8d5b0e"
                }
            ]
        },
        {
            "id": "09b428a4-82d9-4afe-8476-e7504b72a6c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba631f13-6eec-4994-a121-02fbce599084",
            "compositeImage": {
                "id": "59eb3094-83a4-416c-af23-c3b0b4ec4acf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09b428a4-82d9-4afe-8476-e7504b72a6c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6149785d-8b41-4ab2-b1e1-4bbaf80b2c99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09b428a4-82d9-4afe-8476-e7504b72a6c3",
                    "LayerId": "a9526cbf-d916-430b-a456-2d740f8d5b0e"
                }
            ]
        },
        {
            "id": "2f38739f-20de-4049-863d-32ddb368e2f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba631f13-6eec-4994-a121-02fbce599084",
            "compositeImage": {
                "id": "492fe10b-91cb-4b95-8d6a-4993770ba910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f38739f-20de-4049-863d-32ddb368e2f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99fa87b3-47d4-4c0f-a341-276da278ab50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f38739f-20de-4049-863d-32ddb368e2f4",
                    "LayerId": "a9526cbf-d916-430b-a456-2d740f8d5b0e"
                }
            ]
        },
        {
            "id": "4f1f5e93-d826-4f25-8735-38e724e755e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba631f13-6eec-4994-a121-02fbce599084",
            "compositeImage": {
                "id": "5d6a2791-f794-4e8f-96d4-ea3de6be094f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f1f5e93-d826-4f25-8735-38e724e755e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb1c80ed-96a4-40a6-8134-54011f584a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1f5e93-d826-4f25-8735-38e724e755e7",
                    "LayerId": "a9526cbf-d916-430b-a456-2d740f8d5b0e"
                }
            ]
        },
        {
            "id": "3e466db4-2191-46ca-8474-8417eeb62b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba631f13-6eec-4994-a121-02fbce599084",
            "compositeImage": {
                "id": "6b8b76f4-4417-4e8a-848e-c81eb30171d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e466db4-2191-46ca-8474-8417eeb62b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e743b42c-42d8-4c18-8b12-0b466cf2b76f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e466db4-2191-46ca-8474-8417eeb62b38",
                    "LayerId": "a9526cbf-d916-430b-a456-2d740f8d5b0e"
                }
            ]
        },
        {
            "id": "f523d0f6-2901-4222-850a-8f77aa0c9e7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba631f13-6eec-4994-a121-02fbce599084",
            "compositeImage": {
                "id": "b5f3c70e-d1db-477f-9f8e-fac0fdd397e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f523d0f6-2901-4222-850a-8f77aa0c9e7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40fe5ede-9946-4d90-85d5-266b437b1e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f523d0f6-2901-4222-850a-8f77aa0c9e7e",
                    "LayerId": "a9526cbf-d916-430b-a456-2d740f8d5b0e"
                }
            ]
        },
        {
            "id": "05c2e123-4705-455c-95ef-0556368cb584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba631f13-6eec-4994-a121-02fbce599084",
            "compositeImage": {
                "id": "ab0eb926-e78f-47c5-8871-1f1d81fd4353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05c2e123-4705-455c-95ef-0556368cb584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444964a2-0b67-4fff-8957-3a28e8e3e037",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05c2e123-4705-455c-95ef-0556368cb584",
                    "LayerId": "a9526cbf-d916-430b-a456-2d740f8d5b0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "a9526cbf-d916-430b-a456-2d740f8d5b0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba631f13-6eec-4994-a121-02fbce599084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 2
}