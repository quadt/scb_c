{
    "id": "77b2c265-a7de-495c-8f11-7218b43cdaaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "98ea3fc0-2016-49e1-a169-26dece08abea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b2c265-a7de-495c-8f11-7218b43cdaaf",
            "compositeImage": {
                "id": "36f663aa-9088-4981-8269-32e4948e9d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ea3fc0-2016-49e1-a169-26dece08abea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d9c6471-6416-478d-a32a-7d51afc9e5a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ea3fc0-2016-49e1-a169-26dece08abea",
                    "LayerId": "a85495ee-6ace-4482-be85-47814bd72030"
                }
            ]
        },
        {
            "id": "226a5db0-5e99-409d-bd47-58fb5dae96b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b2c265-a7de-495c-8f11-7218b43cdaaf",
            "compositeImage": {
                "id": "40f605b6-ec67-43bf-a56c-dc5b2ec45959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "226a5db0-5e99-409d-bd47-58fb5dae96b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb05591-71f0-4d41-beb2-7c2fe1ed147c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "226a5db0-5e99-409d-bd47-58fb5dae96b0",
                    "LayerId": "a85495ee-6ace-4482-be85-47814bd72030"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "a85495ee-6ace-4482-be85-47814bd72030",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77b2c265-a7de-495c-8f11-7218b43cdaaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 2
}