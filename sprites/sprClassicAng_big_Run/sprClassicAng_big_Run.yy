{
    "id": "84963505-82c3-4e3f-96e7-ffad05300694",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprClassicAng_big_Run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ed7b8d1f-25ca-4cce-8bbf-88e9e874beba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84963505-82c3-4e3f-96e7-ffad05300694",
            "compositeImage": {
                "id": "6e537b6d-5e27-47da-b765-e2bdc8ce203c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed7b8d1f-25ca-4cce-8bbf-88e9e874beba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdd06d1c-6ec2-43a2-a30d-a06e9eaffe8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed7b8d1f-25ca-4cce-8bbf-88e9e874beba",
                    "LayerId": "32de9d2f-3d99-46c5-a17f-c49abf4d1a66"
                }
            ]
        },
        {
            "id": "d358e107-143f-4161-a31c-d871c5752cd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84963505-82c3-4e3f-96e7-ffad05300694",
            "compositeImage": {
                "id": "840f0aad-387b-46ea-a74c-0678320edc30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d358e107-143f-4161-a31c-d871c5752cd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3419b1b-3322-445e-985c-1298da70a98f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d358e107-143f-4161-a31c-d871c5752cd2",
                    "LayerId": "32de9d2f-3d99-46c5-a17f-c49abf4d1a66"
                }
            ]
        },
        {
            "id": "633224c7-ec7a-44b6-b27f-0e7856c23294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84963505-82c3-4e3f-96e7-ffad05300694",
            "compositeImage": {
                "id": "534a5f9c-fe06-4bee-ac98-89e692b9d0c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "633224c7-ec7a-44b6-b27f-0e7856c23294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "457d2b15-3f99-4c9c-9a9d-e1bf06292004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "633224c7-ec7a-44b6-b27f-0e7856c23294",
                    "LayerId": "32de9d2f-3d99-46c5-a17f-c49abf4d1a66"
                }
            ]
        },
        {
            "id": "d0050f7e-589e-481b-b48f-9a07fbb752be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84963505-82c3-4e3f-96e7-ffad05300694",
            "compositeImage": {
                "id": "1c9b12ae-e61f-437c-baf8-e7160fd38175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0050f7e-589e-481b-b48f-9a07fbb752be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "790c2313-2c99-4c18-a48f-20e9550a6700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0050f7e-589e-481b-b48f-9a07fbb752be",
                    "LayerId": "32de9d2f-3d99-46c5-a17f-c49abf4d1a66"
                }
            ]
        },
        {
            "id": "7d7aac03-75f5-432c-9211-fdee3d6c1864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84963505-82c3-4e3f-96e7-ffad05300694",
            "compositeImage": {
                "id": "8a386467-66d5-4c6a-af8a-fcac6ec20fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d7aac03-75f5-432c-9211-fdee3d6c1864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e431a99-16f9-469e-bc22-38b28cecb5b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d7aac03-75f5-432c-9211-fdee3d6c1864",
                    "LayerId": "32de9d2f-3d99-46c5-a17f-c49abf4d1a66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "32de9d2f-3d99-46c5-a17f-c49abf4d1a66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84963505-82c3-4e3f-96e7-ffad05300694",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 9
}