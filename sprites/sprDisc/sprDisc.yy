{
    "id": "fd6520a7-5bd0-4c01-9065-27925b8d18c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDisc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b5c2b50e-dec7-43a5-be6f-e47175150261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6520a7-5bd0-4c01-9065-27925b8d18c7",
            "compositeImage": {
                "id": "0076217e-7a9c-45f6-af0a-1c24bd322f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c2b50e-dec7-43a5-be6f-e47175150261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7629c0-d85f-45d5-857b-67451a14f739",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c2b50e-dec7-43a5-be6f-e47175150261",
                    "LayerId": "4d913860-8517-4f25-95dc-069f50be04b3"
                }
            ]
        },
        {
            "id": "e5f767bd-7451-4095-bc9b-0a35b4001041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6520a7-5bd0-4c01-9065-27925b8d18c7",
            "compositeImage": {
                "id": "9428d024-f0b0-4e8d-a6c4-047d6ca12f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5f767bd-7451-4095-bc9b-0a35b4001041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bdef947-72b6-4eb2-9d84-f420e7fbb114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5f767bd-7451-4095-bc9b-0a35b4001041",
                    "LayerId": "4d913860-8517-4f25-95dc-069f50be04b3"
                }
            ]
        },
        {
            "id": "a9c6e80a-dfd0-437a-86d2-9d223885d3b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6520a7-5bd0-4c01-9065-27925b8d18c7",
            "compositeImage": {
                "id": "8e93256b-ecc5-4b62-931f-785c81186347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9c6e80a-dfd0-437a-86d2-9d223885d3b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c1e857-bdef-449d-88e4-9de85b58ce37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c6e80a-dfd0-437a-86d2-9d223885d3b6",
                    "LayerId": "4d913860-8517-4f25-95dc-069f50be04b3"
                }
            ]
        },
        {
            "id": "6f668899-d543-4796-a170-00a09d346277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6520a7-5bd0-4c01-9065-27925b8d18c7",
            "compositeImage": {
                "id": "2fce46d0-9c97-4751-ae2e-c074465c5f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f668899-d543-4796-a170-00a09d346277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5168b44c-e11e-4867-a950-fc4544545060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f668899-d543-4796-a170-00a09d346277",
                    "LayerId": "4d913860-8517-4f25-95dc-069f50be04b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "4d913860-8517-4f25-95dc-069f50be04b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd6520a7-5bd0-4c01-9065-27925b8d18c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 0
}