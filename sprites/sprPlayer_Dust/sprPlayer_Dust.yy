{
    "id": "49121f30-3cd3-4c76-ac3f-9a2023a7569b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayer_Dust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "599b0578-4637-4c2a-9cf9-73745758b3e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49121f30-3cd3-4c76-ac3f-9a2023a7569b",
            "compositeImage": {
                "id": "4fe1db3e-1b52-482d-bf76-e0332ed20202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599b0578-4637-4c2a-9cf9-73745758b3e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79fea686-09bd-41a8-a236-c20952aa11a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599b0578-4637-4c2a-9cf9-73745758b3e8",
                    "LayerId": "6b82d28f-b329-43a9-83fa-c30314558c61"
                }
            ]
        },
        {
            "id": "5404c27c-4c89-4091-9ea1-b6c3b485333f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49121f30-3cd3-4c76-ac3f-9a2023a7569b",
            "compositeImage": {
                "id": "c97bacc4-1f9f-4632-9d20-0a34ae07baf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5404c27c-4c89-4091-9ea1-b6c3b485333f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c03db1-252c-47ab-a2f1-bc1ad204eabe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5404c27c-4c89-4091-9ea1-b6c3b485333f",
                    "LayerId": "6b82d28f-b329-43a9-83fa-c30314558c61"
                }
            ]
        },
        {
            "id": "62aae07b-8eb9-4796-a48c-868d54646d15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49121f30-3cd3-4c76-ac3f-9a2023a7569b",
            "compositeImage": {
                "id": "2f7032e7-41de-4857-a1f4-b3e06b227d80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62aae07b-8eb9-4796-a48c-868d54646d15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ef90e7-1ddf-401d-9a48-06e851f8c016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62aae07b-8eb9-4796-a48c-868d54646d15",
                    "LayerId": "6b82d28f-b329-43a9-83fa-c30314558c61"
                }
            ]
        },
        {
            "id": "196fe9dd-8fe3-4f07-ac16-abc2651bb2ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49121f30-3cd3-4c76-ac3f-9a2023a7569b",
            "compositeImage": {
                "id": "c38db162-b9c8-4f13-829e-7f7fd6983b89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "196fe9dd-8fe3-4f07-ac16-abc2651bb2ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e81a87c1-638f-48cd-a421-c6d85d267215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "196fe9dd-8fe3-4f07-ac16-abc2651bb2ee",
                    "LayerId": "6b82d28f-b329-43a9-83fa-c30314558c61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "6b82d28f-b329-43a9-83fa-c30314558c61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49121f30-3cd3-4c76-ac3f-9a2023a7569b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}