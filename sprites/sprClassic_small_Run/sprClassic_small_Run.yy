{
    "id": "39490522-090f-4719-9a57-31cfd8d0a67f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprClassic_small_Run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1c6a9470-5a32-4b44-94ef-cc30256a40f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "ee7c3ad5-da1d-4f52-bf39-84871044b566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c6a9470-5a32-4b44-94ef-cc30256a40f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d62704e-4a0a-4698-aae1-9a6982a5ea18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c6a9470-5a32-4b44-94ef-cc30256a40f0",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "7d8b2261-c66b-4093-8f3a-a05c91d39141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "d35f6cad-1102-42ac-8e74-03f6a0ca8fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8b2261-c66b-4093-8f3a-a05c91d39141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8762842d-f078-4e69-9900-143d19d6359c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8b2261-c66b-4093-8f3a-a05c91d39141",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "c158e81f-671d-4564-a8a0-61ff9b76c328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "b125ee61-f9d2-4622-881d-4d94fb49f719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c158e81f-671d-4564-a8a0-61ff9b76c328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35d93dd3-6d81-4854-8168-5c91f7926a58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c158e81f-671d-4564-a8a0-61ff9b76c328",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "d46d3c43-9e8c-4662-8a91-64f8180f33ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "f6e2e2d7-f644-4d61-9f3c-595b1fd377f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d46d3c43-9e8c-4662-8a91-64f8180f33ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c809f1-37ac-4ca2-b3f6-cd945335d509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d46d3c43-9e8c-4662-8a91-64f8180f33ea",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "455abe99-b3bf-4bde-ab2b-20095e67bd61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "120eebed-c612-463a-aabb-90d55e54671d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "455abe99-b3bf-4bde-ab2b-20095e67bd61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27586ab3-d322-42b0-9c49-4b32b09d0a80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "455abe99-b3bf-4bde-ab2b-20095e67bd61",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "5c9fb6fd-dde8-44fd-bd4f-03bc97ab036a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "0ada6fe6-0e4b-4262-9ba9-00f47c92205f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c9fb6fd-dde8-44fd-bd4f-03bc97ab036a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3845607-cca7-4b9d-af96-38159eacf85e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c9fb6fd-dde8-44fd-bd4f-03bc97ab036a",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "1e0f508f-5bf3-4c40-b2d2-99c4da58f080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "d0340b76-1f2b-4397-ad1e-dca853109b63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e0f508f-5bf3-4c40-b2d2-99c4da58f080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe7462a4-f23a-4083-b0e9-72064522c75a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e0f508f-5bf3-4c40-b2d2-99c4da58f080",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "04db7a77-2314-4a04-b567-344ddd383737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "ab21e004-ac09-4fa8-b614-c22588b77e69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04db7a77-2314-4a04-b567-344ddd383737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85fbabf5-033d-41ec-b2c1-417b48cadab2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04db7a77-2314-4a04-b567-344ddd383737",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "061dcd4d-e46e-4ec7-a755-9b8ff3a40680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "3acdeee8-cba3-4d0e-9997-2ca0b4737894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "061dcd4d-e46e-4ec7-a755-9b8ff3a40680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "500d5336-a9cd-474f-bd92-0cbf12d61217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "061dcd4d-e46e-4ec7-a755-9b8ff3a40680",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "0d568415-ef7c-44b6-b30f-5a0668851e17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "15e29b32-d97e-49a0-8e0a-1e1cd2069d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d568415-ef7c-44b6-b30f-5a0668851e17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b45f4e-c760-4ac5-b252-984ea665225e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d568415-ef7c-44b6-b30f-5a0668851e17",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        },
        {
            "id": "70d8a462-120a-4dbc-b530-76dc8b94dc1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "compositeImage": {
                "id": "aac5b9a1-7eec-4a4d-b6eb-e340212ee0b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70d8a462-120a-4dbc-b530-76dc8b94dc1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f9cb23-6767-4d15-b315-0312efb8dbfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70d8a462-120a-4dbc-b530-76dc8b94dc1f",
                    "LayerId": "ecdb44eb-2b55-409b-a579-49b555c1c2e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "ecdb44eb-2b55-409b-a579-49b555c1c2e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39490522-090f-4719-9a57-31cfd8d0a67f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 5
}