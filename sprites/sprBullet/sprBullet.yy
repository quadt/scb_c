{
    "id": "f3cdfd0d-6a58-460b-a4bd-771280673393",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4b443c98-a678-42a6-a53e-dc5ea07bf609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3cdfd0d-6a58-460b-a4bd-771280673393",
            "compositeImage": {
                "id": "2ea3def4-1fb2-4cfb-9a86-1ca07f31df4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b443c98-a678-42a6-a53e-dc5ea07bf609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ced703-aece-4c62-bcb3-2f7085802b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b443c98-a678-42a6-a53e-dc5ea07bf609",
                    "LayerId": "7efc5f93-b041-4be9-9317-fda3e120a867"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "7efc5f93-b041-4be9-9317-fda3e120a867",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3cdfd0d-6a58-460b-a4bd-771280673393",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}