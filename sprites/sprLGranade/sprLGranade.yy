{
    "id": "597dbbb4-1a72-4ccd-ba30-d3e9b598849b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLGranade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8aaee785-e166-4d0c-98cc-316408e43421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "597dbbb4-1a72-4ccd-ba30-d3e9b598849b",
            "compositeImage": {
                "id": "5ecf3b7d-89b0-4bb7-bcda-f7fc4e519da6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aaee785-e166-4d0c-98cc-316408e43421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd395366-8617-4f81-930b-0ec1fbc8e4ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aaee785-e166-4d0c-98cc-316408e43421",
                    "LayerId": "0c1de9b3-3f8a-45cb-b169-8274579b766c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "0c1de9b3-3f8a-45cb-b169-8274579b766c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "597dbbb4-1a72-4ccd-ba30-d3e9b598849b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 1,
    "yorig": 1
}