{
    "id": "1a4a3242-2d35-4953-b5bc-7c7d912bf76c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBazooka",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5ff1d384-5d01-40ee-a87a-12bb87f0326f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a4a3242-2d35-4953-b5bc-7c7d912bf76c",
            "compositeImage": {
                "id": "7203124a-7af5-496e-9ae6-d4ffb7379fe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff1d384-5d01-40ee-a87a-12bb87f0326f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae0fe63-9b65-4d58-88d9-f308ebad59c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff1d384-5d01-40ee-a87a-12bb87f0326f",
                    "LayerId": "179ff4a2-f3d4-47ee-a645-e9b02bf7bc7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "179ff4a2-f3d4-47ee-a645-e9b02bf7bc7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a4a3242-2d35-4953-b5bc-7c7d912bf76c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 3
}