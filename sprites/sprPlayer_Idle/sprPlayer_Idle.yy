{
    "id": "ae73cebb-2c75-4d86-9c81-db28fe47af5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayer_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5b475594-5f24-4ef4-ba11-885cd2c735cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae73cebb-2c75-4d86-9c81-db28fe47af5e",
            "compositeImage": {
                "id": "b6b208a9-32d3-4f20-940e-8cf8cdf26604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b475594-5f24-4ef4-ba11-885cd2c735cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d40cabad-eeb1-4b2b-a4b9-e83c6343af63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b475594-5f24-4ef4-ba11-885cd2c735cd",
                    "LayerId": "66bb9a07-a882-4a44-b4fc-15ac0270db7f"
                }
            ]
        },
        {
            "id": "9e1a0d63-94a1-4973-964b-e5b5a76e95b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae73cebb-2c75-4d86-9c81-db28fe47af5e",
            "compositeImage": {
                "id": "f5e7910f-2c85-4ee7-a566-56a4c26a9dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e1a0d63-94a1-4973-964b-e5b5a76e95b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3196dda-8732-40c8-860a-6c502a43552d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e1a0d63-94a1-4973-964b-e5b5a76e95b6",
                    "LayerId": "66bb9a07-a882-4a44-b4fc-15ac0270db7f"
                }
            ]
        },
        {
            "id": "e7c8d724-726a-4297-ae32-126fc3bcf44a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae73cebb-2c75-4d86-9c81-db28fe47af5e",
            "compositeImage": {
                "id": "c26350e6-d6f7-4ee6-acc1-e18bbac35a2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c8d724-726a-4297-ae32-126fc3bcf44a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b9f7beb-eb0a-48d2-ab02-953fc40737d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c8d724-726a-4297-ae32-126fc3bcf44a",
                    "LayerId": "66bb9a07-a882-4a44-b4fc-15ac0270db7f"
                }
            ]
        },
        {
            "id": "1f07a37c-f155-40ac-8d91-e5261d7290fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae73cebb-2c75-4d86-9c81-db28fe47af5e",
            "compositeImage": {
                "id": "1d9d5d97-d1f4-49f4-a096-424e72c055e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f07a37c-f155-40ac-8d91-e5261d7290fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049dc3f0-9aac-49ed-b303-681cdaf3a9ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f07a37c-f155-40ac-8d91-e5261d7290fb",
                    "LayerId": "66bb9a07-a882-4a44-b4fc-15ac0270db7f"
                }
            ]
        },
        {
            "id": "ee3a2972-cffd-49c0-bb87-bd1c58f9baa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae73cebb-2c75-4d86-9c81-db28fe47af5e",
            "compositeImage": {
                "id": "27bd64cb-ceca-4ea3-bf90-a67d55b005d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee3a2972-cffd-49c0-bb87-bd1c58f9baa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51a38c42-a7f0-4b70-80d6-1620d34fa7b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee3a2972-cffd-49c0-bb87-bd1c58f9baa5",
                    "LayerId": "66bb9a07-a882-4a44-b4fc-15ac0270db7f"
                }
            ]
        },
        {
            "id": "1e2c2d25-0640-4fb6-8480-bbeb36b78e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae73cebb-2c75-4d86-9c81-db28fe47af5e",
            "compositeImage": {
                "id": "c3c86660-1c63-4d2b-afeb-b85b3c5b8a65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e2c2d25-0640-4fb6-8480-bbeb36b78e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbae8774-fa52-4e4f-bb77-efe8a93b3051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e2c2d25-0640-4fb6-8480-bbeb36b78e50",
                    "LayerId": "66bb9a07-a882-4a44-b4fc-15ac0270db7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "66bb9a07-a882-4a44-b4fc-15ac0270db7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae73cebb-2c75-4d86-9c81-db28fe47af5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}