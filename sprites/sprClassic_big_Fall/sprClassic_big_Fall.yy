{
    "id": "64fc2461-c384-403c-952b-12ea44d16130",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprClassic_big_Fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0bdc1707-6333-4f09-8499-eb446ad59a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64fc2461-c384-403c-952b-12ea44d16130",
            "compositeImage": {
                "id": "13b626ba-0e31-42e2-9384-2a1fd02ab4b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bdc1707-6333-4f09-8499-eb446ad59a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "215ba080-2d3e-4ed2-a36b-d56cfd324f4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bdc1707-6333-4f09-8499-eb446ad59a9b",
                    "LayerId": "33659db0-de2d-40c0-b60d-2382c152a697"
                }
            ]
        },
        {
            "id": "b6e22710-6ce0-4b41-8ece-24105701aade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64fc2461-c384-403c-952b-12ea44d16130",
            "compositeImage": {
                "id": "722cbde2-75cb-40c3-a8bd-49e9de49d159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e22710-6ce0-4b41-8ece-24105701aade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "632130cc-95f1-42e4-b23a-541d5a83ea9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e22710-6ce0-4b41-8ece-24105701aade",
                    "LayerId": "33659db0-de2d-40c0-b60d-2382c152a697"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "33659db0-de2d-40c0-b60d-2382c152a697",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64fc2461-c384-403c-952b-12ea44d16130",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 9
}