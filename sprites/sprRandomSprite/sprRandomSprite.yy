{
    "id": "86d4568d-7fc7-4760-a324-f090b5d0219a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRandomSprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "76365a1b-fac6-476c-941f-c618044dafc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86d4568d-7fc7-4760-a324-f090b5d0219a",
            "compositeImage": {
                "id": "ced2c4f9-38c2-4f85-8a63-32f95cbf9dbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76365a1b-fac6-476c-941f-c618044dafc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e0c9499-1891-49c9-b968-fcfecd0ef5fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76365a1b-fac6-476c-941f-c618044dafc4",
                    "LayerId": "c3d3a8db-a6ce-4d80-a8a2-5c5164afbff8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "c3d3a8db-a6ce-4d80-a8a2-5c5164afbff8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86d4568d-7fc7-4760-a324-f090b5d0219a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}