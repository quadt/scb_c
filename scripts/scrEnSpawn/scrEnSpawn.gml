var chos = choose(objClassicEnemy_small, objClassicEnemy_small, objClassicEnemy_big);

if (chos == objClassicEnemy_small){
	var f  = instance_find(objSmallEnSpawner, irandom(instance_number(objSmallEnSpawner) - 1));
} else if (chos == objClassicEnemy_big){
	var f  = instance_find(objBigEnSpawner, irandom(instance_number(objBigEnSpawner) - 1));
}
instance_create_layer(f.x, f.y, "Enemy", chos);