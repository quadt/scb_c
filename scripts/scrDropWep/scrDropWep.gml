/// @arg x
/// @arg y
/// @arg xm
/// @arg ym
/// @arg dir
/// @arg wep

var wep = instance_create_layer(argument[0], argument[1], "Post", objWepDrop);

wep.Dir = argument[4];
wep.wepSelect = argument[5];
wep.XMove = argument[2];
wep.YMove = argument[3];