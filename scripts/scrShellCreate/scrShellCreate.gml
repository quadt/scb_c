/// @arg x
/// @arg y
/// @arg XMove
/// @arg YMove
/// @arg frame

var sh = instance_create_layer(argument[0], argument[1], "Bullets", objShell);
sh.image_angle = 225 + 90 * max(-objPlayer.Dir, 0);
sh.XMove = argument[2];
sh.YMove = argument[3];
sh.image_index = argument[4];