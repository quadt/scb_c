
var f  = instance_find(objCFieldSpawn, irandom(instance_number(objCFieldSpawn) - 1));
if (f.isPlayerCollide == false){
	var W  = f.sprite_width/2;
	var H  = f.sprite_height/2;
	var rX = random_range(f.x-W, f.x+W);
	var rY = random_range(f.y-H, f.y+H);
	instance_create_layer(rX, rY, "Player", objCrate);
} else {
	scrWepRNGSelection();
}