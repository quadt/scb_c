/// @arg x
/// @arg y
/// @arg spd
/// @arg angle
/// @arg type
/// @arg wepPush
/// @arg dmg
/// @arg spr

var b = instance_create_layer(argument[0], argument[1], "Bullets", objBullet);
var p = objPlayer;
p.WPush		 += argument[5];
b.MSpeed      = argument[2];
b.image_angle = argument[3];
b.MAngle      = argument[3];
b.type        = argument[4];
b.DMG		  = argument[6];
b.spr		  = argument[7];
b.sprite_index= argument[7];